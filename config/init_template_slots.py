import json

path_template_action = "template_action.json"
path_template_slots = "template_slots.json"

template_slots = {}

with open(path_template_action, "r") as file_template_action:
    template = json.load(file_template_action)
    for action_name in template:
        action_value = template.get(action_name)
        for packet in action_value:
            text_tts = packet.get("text_tts")
            flag = False
            slot = ""
            for c in text_tts:
                if c == "}":
                    flag = False
                    template_slots[slot] = {
                        "source": "record/input",
                        "value": ["value/name_input"]
                    }
                if flag:
                    slot += c
                if c == "{":
                    flag = True
                    slot = ""

with open(path_template_slots, "w") as file_template_slots:
    json.dump(template_slots, file_template_slots, indent=2)
