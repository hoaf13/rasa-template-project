# Document for defining flow intent

## Define

* Define the flows to be change after combining with context in Recorder
* File: `config/flow_intent.json`

**Example:**
```json
{
  "ANY": {
    "new_intents": [
      {
        "param": "flow_repeat_four_times",
        "value": true,
        "name": "repeat_four_times"
      }
    ]
  },
  "provide_phone": {
    "new_intents": [
      {
        "param": "check_no_phone_provided",
        "value": true,
        "name": "intent_fallback"
      }
    ]
  }
}
```
In example, the keys such as `"ANY"`, `"provide_phone"` represent an `intent` to be changed with each context
in `"new_intents"`. A context includes `"params"` as record value, `"value"` as value in equal term, and `"name"` as
name of new intent. About `records` and `recorder` you can see [Recorder & Records](README.recorder.md)

