import sys
import pickle
import argparse
import os
import json

import networkx as nx
from rasa.core.training.visualization import persist_graph

from config import *
from src.policies.memoization_graph import normalization_intents


def print_log(text):
    sys.stdout.write(text)
    sys.stdout.flush()


def convert_edges_to_stories_with_checkpoint(infile):
    def remove_space_start(text):
        while text.startswith(" "):
            text = text[1:]
        return text

    edges_file = open(infile, "r")
    edges_set = set()
    ke = {}
    for line in edges_file.readlines():
        if line.startswith(">"):
            line = remove_space_start(line).split(">")
            action_pre = remove_space_start(line[1].strip())
            intent = remove_space_start(line[2].strip())
            action_next = remove_space_start(line[3].strip())
            if action_pre not in ke:
                ke[action_pre] = []
            ke[action_pre].append((intent, action_next))
    for action_pre in ke:
        for intent, action_next in ke[action_pre]:
            edges_set.add((action_pre, action_next, intent))
    edges_file.close()
    return edges_set


def write_domain_action_slots(edges_set, list_nodes, entities, limit_slots):
    mp_edges = set()
    list_intents = []
    list_actions = []
    with open(os.path.join("domain.yml"), "w") as file_domain, \
            open(os.path.join("actions.py"), "w") as file_action, \
            open(os.path.join(DIR_TEMPLATE, "action_class.template"), "r") as file_class_template, \
            open(os.path.join(DIR_TEMPLATE, "action_py.template"), "r") as file_action_template, \
            open(PATH_CONFIG_LIMITS_SLOTS, "w") as file_limit_slots, \
            open(os.path.join(DIR_OUTPUT, "initial_template_actions.json"), "w") as file_initial_template_actions:
        file_domain.write("session_config:\n")
        file_domain.write(f"  session_expiration_time: {SESSION_EXPIRATION_TIME}\n")
        file_domain.write(f"  carry_over_slots_to_new_session: {CARRY_OVER_SLOTS_TO_NEW_SESSION}\n\n")
        file_domain.write("intents:\n")
        for u, v, intent in edges_set:
            intent = intent.split("{")[0]
            if intent != "None" and intent not in mp_edges:
                mp_edges.add(intent)
                list_intents.append(intent)
        list_intents.sort()
        for intent in list_intents:
            file_domain.write(f"- {intent}\n")
        file_domain.write("\n")
        file_domain.write("actions:\n")
        for node in list_nodes:
            if node != "START" and node != "END":
                list_actions.append(node)
        list_actions.sort()
        list_actions.append("action_quit")

        content = file_action_template.read()
        file_action.write(content)

        lines = file_class_template.readlines()
        template_actions = []
        for action in list_actions:
            file_domain.write(f"- {action}\n")
            for line in lines:
                if "ACTION_NAME" in line:
                    line = line.replace("ACTION_NAME", action)
                if "ACTION_CLASS_NAME" in line:
                    line = line.replace("ACTION_CLASS_NAME", get_class_name(action))
                file_action.write(line)
            file_action.write("\n\n\n")
            template_action = {
                action: [
                    {
                        "status": "CHAT",
                        "text": "",
                        "text_tts": ""
                    }
                ]
            }
            template_actions.append(template_action)
        json.dump(template_actions, file_initial_template_actions, indent=2)
        if len(entities) > 0:
            file_domain.write("\nentities:\n")
            for entity in entities:
                file_domain.write(f"- {entity}\n")
            file_domain.write("\nslots:\n")
            for entity in entities:
                file_domain.write(f"  {entity}:\n")
                file_domain.write(f"    type: text\n")
        json.dump(limit_slots, file_limit_slots, indent=2)


def get_class_name(action):
    words = action.split("_")
    name = ""
    for w in words:
        name += w.capitalize()
    return name


def build_graph_from_edges(edges_set, unmerge_nodes=None):
    if unmerge_nodes is None:
        unmerge_nodes = []
    nodes_set = set()
    parents = set()
    list_graph = []
    for u, v, intent in edges_set:
        nodes_set.add(u)
        nodes_set.add(v)
        parents.add(u)

    for u in nodes_set:
        if u not in parents:
            edges_set.add((u, "END", "None"))
    nodes_set.add("END")

    entities_set = set()
    limit_slots = {}
    for u, v, intent in edges_set:
        node = {
            'pre_node': u,
            'cur_node': v,
            'intent': '/' + intent,
            'count': 0,
        }
        name_intent, entities = normalization_intents(intent)
        if entities is not None:
            for entity in entities:
                entities_set.add(entity)

                if name_intent not in limit_slots:
                    limit_slots[name_intent] = []
                if entity not in limit_slots[name_intent]:
                    limit_slots[name_intent].append(entity)
        list_graph.append(node)
    list_edges = list(edges_set)
    list_nodes = list(nodes_set)
    entities = list(entities_set)

    write_domain_action_slots(edges_set, list_nodes, entities, limit_slots)

    multi_di_graph = nx.MultiDiGraph()
    # multi_di_graph.add_nodes_from(nodes)
    unmerge_node_count_dict = dict()
    for unmerge_node in unmerge_nodes:
        unmerge_node_count_dict[unmerge_node] = 0

    traveled_path = dict()
    for u, v, intent in list_edges:
        path = (u, intent)
        if path in traveled_path and traveled_path[path] != v:
            print_log("\n[ERROR] Found 2 conflict path:\n")
            print_log("[ERROR] Path1: {} ---{}--> {}\n".format(u, intent, v))
            print_log("[ERROR] Path1: {} ---{}--> {}\n".format(u, intent, traveled_path[path]))
            exit(1)
        traveled_path[path] = v
        if v in unmerge_node_count_dict:
            unmerge_node_count_dict[v] += 1
            v = "{}[{}]".format(v, unmerge_node_count_dict[v])
        multi_di_graph.add_nodes_from([u, v])
        multi_di_graph.add_edge(u, v, **{"label": intent})
    # exp_g = nx.nx_pydot.to_pydot(G)
    # print ("graph_list: ", graph_list)
    return list_graph, list_edges, list_nodes, multi_di_graph


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("infile", type=str, help="Input stories with edges")
    parser.add_argument("--config-graph", type=str, help="Output pickle graph config file",
                        default=PATH_CONFIG_GRAPH)
    parser.add_argument("--output-html", type=str, help="Output html graph", default=PATH_OUTPUT_GRAPH_HTML)
    parser.add_argument("--unmerge-nodes", type=str, help="Unmerge action node in graph, separate by comma",
                        default='action_sorry')

    args = parser.parse_args()
    assert os.path.exists(args.infile), "story-infile-edges {} not found!".format(args.infile)

    print_log("Reading edges from {} - \n".format(args.infile))
    edges_init = convert_edges_to_stories_with_checkpoint(args.infile)

    print_log("Building graph edges - ")
    graph_list, edges, nodes, graphviz = build_graph_from_edges(edges_init,
                                                                unmerge_nodes=args.unmerge_nodes.split(','))
    print_log("{} edges - {} nodes\n".format(len(edges), len(nodes)))

    pickle.dump(graph_list, open(args.config_graph, "wb"))
    print_log("Dump graph pickle in {}\n".format(args.config_graph))

    persist_graph(graphviz, args.output_html)
    print_log("Print graph to {}\n".format(args.output_html))
