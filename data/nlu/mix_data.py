data_priority = "main_data/no_repair.md"
data_append = ["main_data/deny_confirm.md"]

name_priority = data_priority.split("/")[-1].replace(".md", "")

append_point = "append_all"

import random

random.seed(666)

for data_tmp in data_append:
    name_tmp = data_tmp.split("/")[-1].replace(".md", "")
    name_new = f"mix_data/{name_priority}_mix_{name_tmp}_{append_point}_new.md"
    with open(name_new, "w") as fo:
        fo.write(f"## intent:{name_priority}\n")
        fi_1 = open(data_tmp, "r")
        fi_2 = open(data_priority, "r")
        lines_1 = []
        for line in fi_1.readlines()[1:]:
            if len(line.split()) < 6:
                lines_1.append(line)
        lines_2 = fi_2.readlines()[1:]
        n = 0
        while len(lines_2) < len(lines_1):
            lines_2.append(lines_2[n])
            n += 1
        n = 0
        while len(lines_1) < len(lines_2):
            lines_1.append(lines_1[n])
            n += 1
        random.shuffle(lines_1)
        random.shuffle(lines_2)
        n = len(lines_1)
        for i in range(n):
            l1 = lines_1[i].replace("-", "").strip()
            l2 = lines_2[i].replace("-", "").strip()
            if append_point == "append_before":
                fo.write(f"- {l1} {l2}\n")
            elif append_point == "append_after":
                fo.write(f"- {l2} {l1}\n")
            elif append_point == "append_all":
                fo.write(f"- {l1} {l2}\n")
                fo.write(f"- {l2} {l1}\n")
