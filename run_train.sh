#!/bin/bash

. run_config.sh
export CUDA_VISIBLE_DEVICES=-1

nohup $bin/rasa train --debug > $PATH_LOG/rasa_train.log &
