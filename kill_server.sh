#!/bin/bash

. run_config.sh

pid_action=$(cat $PATH_PID/rasa_action.pid)
echo "PID ACTION: $pid_action"
ps -o pid= --ppid $pid_action
kill -9 $(ps -o pid= --ppid $pid_action)
kill -9 $pid_action

pid_server=$(cat $PATH_PID/rasa_server.pid)
echo "PID SERVER: $pid_server"
ps -o pid= --ppid $pid_server
kill -9 $(ps -o pid= --ppid $pid_server)
kill -9 $pid_server

rm $PATH_PID/*.pid