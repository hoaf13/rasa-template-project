import os

SESSION_EXPIRATION_TIME = 60
CARRY_OVER_SLOTS_TO_NEW_SESSION = "true"
DB_TEST_MODE = False
MAX_INTENTS = 100000
DEFAULT_NAME_INTENT_FALLBACK = "intent_fallback"
DEFAULT_ENCODING = "utf8"

DIR_CONFIG = "config"
DIR_RESOURCES = "resources"
DIR_CONST = os.path.join(DIR_RESOURCES, "const")
PATH_CONFIG = os.path.join(DIR_CONFIG, "config.json")
PATH_CONFIG_FLOW_INTENT = os.path.join(DIR_CONFIG, "flow_intent.json")
PATH_CONFIG_GRAPH = os.path.join(DIR_CONFIG, "graph")
PATH_CONFIG_LIMITS_SLOTS = os.path.join(DIR_CONFIG, "limit_slots.json")
PATH_CONFIG_REGEX = os.path.join(DIR_CONFIG, "regex.json")
PATH_CONFIG_TEMPLATE_ACTION = os.path.join(DIR_CONFIG, "template_action.json")
PATH_CONFIG_USER_INFO = os.path.join(DIR_CONFIG, "user_info.json")
PATH_CONFIG_TEMPLATE_SLOTS = os.path.join(DIR_CONFIG, "template_slots.json")
PATH_CONFIG_FALLBACK_WORDS = os.path.join(DIR_CONST, "fallback_words.txt")
PATH_CONFIG_STOP_WORDS = os.path.join(DIR_CONST, "stop_words.json")
DIR_OUTPUT = os.path.join(DIR_RESOURCES, "outputs")
DIR_TEMPLATE = os.path.join(DIR_RESOURCES, "templates")
PATH_OUTPUT_GRAPH_HTML = os.path.join(DIR_OUTPUT, "graph.html")
PATH_DOMAIN = "domain.yml"
PATH_W2V = "/home/smartcall/wiki/wiki.vi.vec"
