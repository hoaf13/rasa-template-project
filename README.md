# Template Callbot NLP

An extended project of Rasa Core, in which we re-built NLU-model, Graph of conversation, Policy, Context, etc.

## Environment

```
/home/smartcall/anaconda3/envs/rasa-redis-options/bin
```

## Config

* Config `PORT`, `ENV`, `PATH` in `run_config.sh`
* Config `PORT_ACTION` in `endpoints.yml`
* After build (down hear) let config other config in path `config/`:
  `config.json`, `limit_slots.json`

## Preparing

* graph: [Graph](docs/README.graph.md) - `resources/graphs/graph.md`
* data: [Data for Classifier](docs/README.classifier.md) `data/nlu/*.md`

## Build

* Build graph: after preparing the graph, run file `run_graph.sh` which will build graph from `resources/graph.md`
  to `config/graph`, make `domain.yml`, make `actions.py`, and make config `limit_slots.json`

```
./run_graph.sh
```

* Build model: after preparing the data, run file `run_train.sh` to build model NLU which will create a model with
  version tag in path `models/`

```
./run_train.sh
```

## Coding

* `recorder`: [Recorder & Records](docs/README.recorder.md) - context for the conversation
* `flow_intent.json`: [Change Flow by Context](docs/README.flow.md) - change intent by the context
* `regex.json`: a part of [Named Entity Recognition](docs/README.entity.md) - predict intent with regex
* `template_action.json`: [The response packet of action](docs/README.template.md) define `text`, `text_tts`, `status`
  , `speed`, `sip forward`, ... for the packet which pass to the master process
* `template_slots.json`: after making `template_action.json` run `python make_template_slots.py` for the first time to
  init all template slots

## Run server

* We need to create folders included `resources/logs/`, `resources/pid/` before starting
* Note to ensure `PORT_ACTION` and `PORT_SERVER` have not been run before
* Run:

```
./run_server.sh
```

* Logs: `resources/logs`