from typing import Text

from rasa_sdk import Action
from rasa_sdk.events import Restarted
import json
import logging
import copy

from src.database.context_store import RedisStore
from config import (
    PATH_CONFIG_TEMPLATE_ACTION,
    PATH_CONFIG
)
from src.database.context_store import ContextStore

logger = logging.getLogger(__name__)

with open(PATH_CONFIG_TEMPLATE_ACTION, "r") as file_action:
    action_template = json.load(file_action)

with open(PATH_CONFIG, "r") as file_config:
    all_config = json.load(file_config)
    query_actions = all_config.get("query_actions")

redis_more_store = RedisStore()


async def execute_normal_action(tracker, cur_action):
    """
    execute action for all actions includes:
    - update records from tracker
    - get action response from action_template
    - format response message
    :param tracker: rasa tracker
    :param cur_action: current action
    :return: response
    """

    responses = copy.deepcopy(action_template[cur_action])
    recorder = redis_more_store.get_data(tracker.sender_id).tickets["recorder"]
    if cur_action in query_actions:
        await recorder.update_async_method()
        redis_more_store.save_data(ContextStore(tracker.sender_id, recorder=recorder))
    record = recorder.get_current_record()
    cur_intent = recorder.cur_intent

    def get_action_response():
        response_ans = responses[0]
        for response_id in responses:
            if "properties" in response_id:
                properties = response_id["properties"]
                check_properties = True
                for p in properties:
                    value_property = properties[p]
                    if p in record:
                        value_recorded = record[p]
                        if value_recorded != value_property:
                            check_properties = False
                            break
                if check_properties:
                    response_ans = response_id
                    break
            else:
                response_ans = response_id
                break
        return response_ans

    def format_text(text_format, record_tmp):
        for key in record_tmp:
            if isinstance(record_tmp[key], dict):
                pass
            else:
                text_format = text_format.replace("{" + str(key) + "}", str(record_tmp[key]))
        return text_format

    logger.debug(f"{tracker.sender_id} - Record after executed action: {json.dumps(record, indent=4)}")

    response = get_action_response()
    text = response["text"]
    text_tts = response["text_tts"]

    response["text"] = format_text(text, record).replace('"', '')
    response["text_tts"] = format_text(text_tts, record).replace('"', '')
    response["action"] = cur_action
    response["intent"] = cur_intent
    response["confidence"] = tracker.current_state().get("latest_message").get("intent").get("confidence")
    restart = response["status"] == "END" or response["status"] == "FORWARD"
    return json.dumps(response), restart


class ActionBusy(Action):
    def name(self) -> Text:
        return "action_busy"

    async def run(self, dispatcher, tracker, domain):
        response, restart = await execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []


class ActionStart(Action):
    def name(self) -> Text:
        return "action_start"

    async def run(self, dispatcher, tracker, domain):
        response, restart = await execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []


class ActionQuit(Action):
    def name(self) -> Text:
        return "action_quit"

    async def run(self, dispatcher, tracker, domain):
        response, restart = await execute_normal_action(tracker, self.name())
        dispatcher.utter_message(text=response)
        if restart:
            return [Restarted()]
        return []


