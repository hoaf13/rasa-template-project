import json
import logging

from typing import Text, Optional, Dict, Any

from src.context.recorder import Recorder
from config import PATH_CONFIG
from src.database.redis_connection import RedisTable

logger = logging.getLogger(__name__)


class ContextStore:
    """
    manager list info need store to DB
    """

    def __init__(self, conversation_id, recorder=None):
        """
        Create list info need store to DB
        :param conversation_id:
        :param recorder: recorder object to store
        """
        self.conversation_id = conversation_id
        if recorder is None:
            recorder = Recorder(self.conversation_id)
        self.tickets = {
            "recorder": recorder
        }

    def dumps(self):
        tickets = {}
        for key in self.tickets:
            tickets[key] = self.tickets[key].dumps()
        return json.dumps(dict(conversation_id=self.conversation_id, tickets=tickets))

    @classmethod
    def from_dict(cls, data: Dict[Text, Any]) -> "ContextStore":
        tickets = data.get("tickets")
        recorder = Recorder.from_dict(data.get("conversation_id"), tickets.get("recorder"))

        return cls(data.get("conversation_id"),
                   recorder
                   )

    def get_ticket(self, ticket_name):
        return self.tickets[ticket_name]


class RedisStore:
    """Redis store for ticket locks."""

    def __init__(self):
        with open(PATH_CONFIG, "r") as file_config:
            config = json.load(file_config)
            config_redis = config.get("redis")
            self.redis_table = RedisTable(config=config_redis, table="recorder")

    def get_data(self, conversation_id: Text) -> "ContextStore":
        serialised_data = self.redis_table.get(conversation_id)
        if not serialised_data:
            new_data = ContextStore(conversation_id)
            self.save_data(new_data)
            return new_data
        return ContextStore.from_dict(json.loads(serialised_data))

    def save_data(self, data: ContextStore) -> None:
        self.redis_table.set(data.conversation_id, data.dumps())

    def delete(self, conversation_id: Text) -> None:
        self.redis_table.delete(conversation_id)
