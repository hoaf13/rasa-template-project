import json
import logging

from config import (
    PATH_CONFIG,
    PATH_CONFIG_USER_INFO,
    DB_TEST_MODE
)
from src.database.database_connection import DatabaseConnection

logger = logging.getLogger(__name__)


class UserInfoStore:
    def __init__(self):
        with open(PATH_CONFIG, "r") as file_config:
            config = json.load(file_config).get("database")
            self.connect = DatabaseConnection(host=config.get("host"),
                                              username=config.get("username"),
                                              password=config.get("password"),
                                              db=config.get("db"),
                                              port=config.get("port"))
            self.user_information_table = config.get("user_information_table")
        with open(PATH_CONFIG_USER_INFO, "r") as file_slot:
            self.config_slots = json.load(file_slot)

        self.PARAMS_INT = []
        self.PARAMS_STR = []
        self.PARAMS_ARR = []
        self.PARAMS_BOOL = []
        for key in self.config_slots:
            value = self.config_slots.get(key)
            if isinstance(value, int):
                self.PARAMS_INT.append(key)
            elif isinstance(value, str):
                self.PARAMS_STR.append(key)
            elif isinstance(value, list):
                self.PARAMS_ARR.append(key)
            elif isinstance(value, bool):
                self.PARAMS_BOOL.append(key)

    def get(self, sender_id):
        if DB_TEST_MODE:
            sender_id = "test"
        if sender_id is None:
            return {}
        list_slots = [key for key in self.config_slots]
        keyword = ",".join(list_slots)
        sql = f"SELECT {keyword} FROM "
        sql += f"{self.user_information_table}" + " WHERE id_conversation = %s"
        connection = self.connect.get_connection()
        cursor = connection.cursor()
        connection.commit()
        cursor.execute(sql, [sender_id])
        query_result = cursor.fetchall()
        cursor.close()
        connection.close()
        answer = {}
        if query_result is not None and len(query_result) > 0:
            for x in query_result:
                for i, name in enumerate(list_slots):
                    answer[name] = {}
                    if name in self.PARAMS_ARR:
                        answer[name] = self.process_array_param(x[i])
                    elif name in self.PARAMS_INT:
                        answer[name] = self.process_int_param(x[i])
                    elif name in self.PARAMS_BOOL:
                        answer[name] = self.process_bool_param(x[i])
                    else:
                        answer[name] = self.process_string_param(x[i])
        answer = self.normalize_params(answer)
        logger.debug(f"{sender_id} - Get information of user {sender_id} successful")
        return answer

    @staticmethod
    def process_array_param(text):
        try:
            text = f"{text}"
            text = text \
                .replace("\\", "") \
                .replace("\"", "") \
                .replace(" ,", ",") \
                .replace(", ", ",") \
                .replace("[", "[\"") \
                .replace("]", "\"]") \
                .replace(",", "\",\"")
            arr_item = json.loads(text)
            return arr_item
        except Exception as e:
            logger.debug(f"Process array param error: {e}")
            return None

    @staticmethod
    def process_int_param(x):
        try:
            int_item = int(x)
        except Exception as e:
            logger.debug(f"Process int param error: {e}")
            int_item = None
        return int_item

    @staticmethod
    def process_bool_param(x):
        bool_item = None
        if x == "true":
            bool_item = True
        elif x == "false":
            bool_item = False
        return bool_item

    @staticmethod
    def process_string_param(x):
        return str(x).strip()

    @staticmethod
    def normalize_params(answer):
        return answer

    def update_report(self, sender_id, report):
        sql = f"UPDATE {self.user_information_table}"
        sql += " SET report='{}' WHERE id_conversation LIKE '{}'".format(report, sender_id)
        connection = self.connect.get_connection()
        cursor = connection.cursor()
        cursor.execute(sql)
        connection.commit()
        cursor.close()
        connection.close()
        logger.debug(f"{sender_id} - Update report to DB: {report}")
