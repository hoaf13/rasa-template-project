import logging

logger = logging.getLogger(__name__)


class RedisTable:
    def __init__(self, config, table):
        self.name_db = config.get("name_db")
        self.type_db = config.get("type_db")
        password = config.get("password")
        self.socket_timeout = config.get("socket_timeout")
        self.use_ssl = config.get("use_ssl")
        self.default_ttl = config.get("default_ttl")
        self.table = table
        logger.debug(f"Make a connection to Redis {self.type_db} with db {self.name_db}.{self.table}")
        if self.type_db == "cluster":
            from rediscluster import RedisCluster
            startup_nodes = config.get("cluster_nodes")
            self.red = RedisCluster(startup_nodes=startup_nodes,
                                    decode_responses=True,
                                    password=password,
                                    socket_timeout=self.socket_timeout)
        elif self.type_db == "sentinel":
            from redis.sentinel import Sentinel
            nodes = config.get("sentinel_nodes")
            nodes_tuple = []
            for node in nodes:
                host = node.get("host")
                port = int(node.get("port"))
                nodes_tuple.append((host, port))
            self.red = Sentinel(nodes_tuple,
                                password=password,
                                socket_timeout=self.socket_timeout)
            self.master = config.get("sentinel_master_name")
        else:  # normal redis
            import redis
            host = config.get("host")
            port = config.get("port")
            db = config.get("db")
            self.red = redis.StrictRedis(
                host=host,
                port=int(port),
                db=int(db),
                password=password,
                ssl=self.use_ssl,
                socket_timeout=self.socket_timeout
            )

    def key_format(self, key):
        return f"{self.name_db}-{self.table}-{key}"

    def get_cursor(self):
        if self.type_db == "sentinel":
            return self.red.master_for(self.master, socket_timeout=self.socket_timeout)
        else:
            return self.red

    def set(self, key, data, ex=None):
        if ex is None:
            ex = self.default_ttl
        return self.get_cursor().set(self.key_format(key), data, ex)

    def get(self, key):
        return self.get_cursor().get(self.key_format(key))

    def delete(self, key):
        return self.get_cursor().delete(self.key_format(key))
