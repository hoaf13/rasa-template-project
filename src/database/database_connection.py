import MySQLdb

import traceback
import logging

logger = logging.getLogger(__name__)


class DatabaseConnection:
    def __init__(self, host, username, password, db, port):
        self.host = host
        self.username = username
        self.password = password
        self.db = db
        self.port = port

    def get_connection(self):
        logger.debug("Attempt to connect database")
        try:
            connection = MySQLdb.connect(
                host=self.host,
                port=self.port,
                user=self.username,
                passwd=self.password,
                db=self.db
            )
            connection.set_character_set('utf8')
            connection.autocommit(True)
            logger.debug("Connect to database successful")
            return connection
        except Exception as e:
            logger.error(f"Fail to connect database: {e}")
            logger.error(traceback.format_exc())
            exit(1)
        return None
