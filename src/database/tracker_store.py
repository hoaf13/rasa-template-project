import json
import logging
from typing import Optional, Text, Iterable
import time
import copy

from rasa.core.trackers import DialogueStateTracker
from rasa.core.tracker_store import TrackerStore
from rasa.core.brokers.broker import EventBroker

from config import PATH_CONFIG
from src.database.database_connection import DatabaseConnection

from src.database.context_store import RedisStore
from src.database.redis_connection import RedisTable

logger = logging.getLogger(__name__)


class SQLTrackerStoreCustom(TrackerStore):
    """Store which can save and retrieve trackers from an SQL database."""

    def __init__(
            self,
            domain,
            host="localhost",
            port=6379,
            db=0,
            password: Optional[Text] = None,
            event_broker: Optional[EventBroker] = None,
            record_exp: Optional[float] = None,
            use_ssl: bool = False,
    ):

        self.redis_more_store = RedisStore()

        with open(PATH_CONFIG, "r") as file_config:
            config = json.load(file_config).get("database")
            self.connect = DatabaseConnection(host=config.get("host"),
                                              username=config.get("username"),
                                              password=config.get("password"),
                                              db=config.get("db"),
                                              port=config.get("port"))
            self.history_table = config.get("history_table")

        with open(PATH_CONFIG, "r") as file_config:
            config = json.load(file_config)
            self.config = config
            config_redis = config.get("redis")
            self.red = RedisTable(config=config_redis, table="tracker")

        self.record_exp = record_exp
        logger.debug("Create Custom tracker store successful")

        super().__init__(domain, event_broker)

    def insert_history(self, sender_id, pre_action, timestamp, intent_name, action_name,
                       text_user, text_bot):
        sql = "INSERT INTO "
        sql += self.history_table + " (sender_id, pre_action, timestamp, intent_name, action_name,"
        sql += " text_user, text_bot) VALUES (%s, %s, %s, %s, %s, %s, %s)"
        val = (sender_id, pre_action, timestamp, intent_name, action_name,
               text_user, text_bot)
        connection = self.connect.get_connection()
        cursor = connection.cursor()
        cursor.execute(sql, val)
        connection.commit()
        cursor.close()
        connection.close()

    def keys(self) -> Iterable[Text]:
        """Return keys of the Redis Tracker Store"""
        return self.red.keys()

    def retrieve(self, sender_id: Text) -> Optional[DialogueStateTracker]:
        """Create a tracker from all previously stored events."""
        stored = self.red.get(sender_id)
        if stored is not None:
            return self.deserialise_tracker(sender_id, stored)
        else:
            return None

    def save(self, tracker: DialogueStateTracker, timeout=None) -> None:
        """Update database with events from the current conversation."""
        sender_id = tracker.sender_id
        if self.event_broker:
            self.stream_events(tracker)
        if not timeout and self.record_exp:
            timeout = self.record_exp
        serialised_tracker = self.serialise_tracker(tracker)
        self.red.set(sender_id, serialised_tracker, ex=timeout)
        logger.debug(
            "{} - Tracker stored to Redis db".format(sender_id)
        )

        # Delete recorder in redis db if conversation was over
        recorder = self.redis_more_store.get_data(sender_id).tickets["recorder"]
        if recorder.pre_action is None:
            pre_action = "START"
        else:
            pre_action = copy.deepcopy(recorder.pre_action)
        if recorder.cur_action in self.config.get('finish_actions'):
            self.redis_more_store.delete(sender_id)
            self.red.delete(sender_id)

        # Get a pair of messages include pre_action, intent, cur_action, text_bot, text_user
        intent_real = None
        action_real = None
        text_user = None
        text_bot = None
        events = tracker.events
        for event in events:
            data = event.as_dict()
            talker = data.get('event')
            if talker == "user":
                text_user = data.get("text")
            if talker == "bot":
                text_bot = data.get("text")
            intent = data.get("parse_data", {}).get("intent", {}).get("name")
            action = data.get("name")
            if action is not None and action != "action_listen":
                action_real = action
            if intent is not None:
                intent_real = intent

        # Insert a pair of messages to history which use for view log called conversation
        timestamp = time.time()
        if intent_real is not None and action_real is not None and text_bot is not None and text_user is not None:
            # insert data into database mysql
            try:
                self.insert_history(sender_id, pre_action, timestamp, intent_real, action_real,
                                    text_user, text_bot)
                logger.debug(f"{sender_id} - Insert a pair of messages to db.history successful")
            except Exception as e:
                import traceback
                logger.debug(f"{sender_id} - Cannot insert a pair of messages to db.history - follow this error: {e}")
                logger.debug(traceback.format_exc())
