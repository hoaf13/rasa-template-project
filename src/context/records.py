from typing import Any, Dict, Iterator, List, Optional, Text
import logging
import re
import os, time
import json

os.environ['TZ'] = 'Asia/Ho_Chi_Minh'
time.tzset()

logger = logging.getLogger(__name__)


class ImplRecord:
    def __init__(self):
        self.record = None

    def update(self, recorder) -> None:
        """
        check_update and update from values of recorder
        :param recorder: Recorder
        :return: None
        """
        raise NotImplementedError("An record must implement its update method")

    def reset(self) -> None:
        """
        reset value of item record when recorder run reset
        :return: None
        """
        self.record = None

    def set_record(self, record):
        self.record = record

    def get_value(self):
        return self.record


class ImplRecordAsync:
    def __init__(self):
        self.record = None

    async def update(self, recorder) -> None:
        """
        check_update and update from values of recorder
        :param recorder: Recorder
        :return: None
        """
        raise NotImplementedError("An record must implement its update method")

    def reset(self) -> None:
        """
        reset value of item record when recorder run reset
        :return: None
        """
        self.record = None

    def set_record(self, record):
        self.record = record

    def get_value(self):
        return self.record


class CheckActionRepeat(ImplRecord):
    def __init__(self):
        super().__init__()
        self.record = False

    def update(self, recorder) -> None:
        pre_action = recorder.pre_action
        cur_action = recorder.cur_action
        self.record = pre_action == cur_action


class GetCurrentIntent(ImplRecord):
    def __init__(self):
        super().__init__()
        self.record = None

    def update(self, recorder) -> None:
        self.record = recorder.cur_intent


class GetPreIntent(ImplRecord):
    def __init__(self):
        super().__init__()
        self.record = {
            "pre_intent": None,
            "cur_intent": None
        }

    def update(self, recorder) -> None:
        self.record["pre_intent"] = self.record["cur_intent"]
        self.record["cur_intent"] = recorder.cur_intent


class GetPreAction(ImplRecord):
    def __init__(self):
        super().__init__()
        self.record = None

    def update(self, recorder) -> None:
        self.record = recorder.pre_action


class GetReport(ImplRecord):
    def __init__(self):
        super().__init__()
        self.record = {
        }

    def update(self, recorder) -> None:
        pass


class CountAction(ImplRecord):
    def __init__(self):
        super().__init__()
        self.record = 1

    def update(self, recorder) -> None:
        if recorder.cur_action == recorder.pre_action:
            self.record += 1
        else:
            self.record = 1


class FlowRepeatFourTimes(ImplRecord):
    def __init__(self):
        super().__init__()
        self.record = False

    def update(self, recorder) -> None:
        if recorder.get_current_record().get("count_action") >= 4:
            self.record = True
        else:
            self.record = False
