from rasa_sdk import Tracker
import json
import copy

from src.database.user_store import UserInfoStore
from src.context.records import *
from config import PATH_CONFIG_USER_INFO

records_init = {
    "count_action": CountAction(),
    "check_repeat": CheckActionRepeat(),
    "get_pre_intent": GetPreIntent(),
    "get_pre_action": GetPreAction(),
    "get_cur_intent": GetCurrentIntent(),
    "flow_repeat_four_times": FlowRepeatFourTimes(),

    "get_report": GetReport()
}
query_init = [
]
user_store = UserInfoStore()


class Recorder:
    """
    record of conversation by sender_id
    """

    def __init__(self, sender_id) -> None:
        self.query_records = copy.deepcopy(query_init)
        self.sender_id = sender_id
        self.state = True
        self.old_action = None
        self.pre_action = None
        self.cur_action = None
        self.cur_intent = None
        self.last_message = None
        self.custom_records = copy.deepcopy(records_init)
        self.slots_record = {}
        with open(PATH_CONFIG_USER_INFO, encoding='utf8') as file_user_info:
            self.user_info = json.load(file_user_info)
        self.get_information_from_database(sender_id)
        self.slots_record_store = copy.deepcopy(self.slots_record)
        self.custom_records_store = copy.deepcopy(self.custom_records)
        self.old_action_store = copy.deepcopy(self.old_action)
        self.pre_action_store = copy.deepcopy(self.pre_action)
        self.cur_action_store = copy.deepcopy(self.cur_action)

    def get_information_from_database(self, sender_id):
        """
        get information of user from database -> set to self.user_info
        if any info is None -> set to default value from user_info.json
        :param sender_id:
        :return: None
        """
        info = user_store.get(sender_id)
        for key in info:
            value = info[key]
            if value is not None and value != "null" and value != "NULL" and value != "None":
                self.user_info[key] = value

    def update_from_tracker(self, tracker: Tracker) -> None:
        """
        from tracker, update params
        self.pre_action
        self.slots_record
        :param tracker:
        :return:
        """

        message_out = tracker.current_state().get("latest_message").get("text")
        self.last_message = message_out
        # current_slots = tracker.current_slot_values()
        current_entities = tracker.current_state().get("latest_message").get("entities")
        current_slots = {}
        for entity in current_entities:
            name_slot = entity.get("entity")
            value = entity.get("value")
            current_slots[name_slot] = value

        self.slots_record_store = copy.deepcopy(self.slots_record)
        for slot in current_slots:
            value = current_slots[slot]
            value = value if value is None else str(value)
            if slot not in self.slots_record:
                self.slots_record[slot] = []
            self.slots_record[slot].append(value)
        for slot in self.slots_record:
            if slot not in current_slots:
                self.slots_record[slot].append(None)

    def update_report_to_db(self) -> None:
        report = self.custom_records["get_report"].get_value()
        report = json.dumps(report, ensure_ascii=False).encode('utf8').decode()
        user_store.update_report(self.sender_id, report)

    def update(self, tracker: Tracker, cur_action: Text, cur_intent: Text, update_record=True) -> None:
        """"""
        self.cur_intent = copy.deepcopy(cur_intent)
        if cur_intent is not None:
            self.old_action = copy.deepcopy(self.pre_action)
            self.pre_action = copy.deepcopy(self.cur_action)
            self.cur_action = copy.deepcopy(cur_action)
        self.update_from_tracker(tracker)
        if update_record:
            for name_record in self.custom_records:
                if name_record not in self.query_records:
                    self.custom_records[name_record].update(self)

    async def update_async_method(self):
        for name_record in self.query_records:
            if name_record in self.custom_records:
                await self.custom_records[name_record].update(self)

    def store(self):
        """
        store value of some attributes of recorder
        :return: None
        """
        self.slots_record_store = copy.deepcopy(self.slots_record)
        self.custom_records_store = copy.deepcopy(self.custom_records)
        self.old_action_store = copy.deepcopy(self.old_action)
        self.pre_action_store = copy.deepcopy(self.pre_action)
        self.cur_action_store = copy.deepcopy(self.cur_action)

    def roll_back(self):
        """
        Rollback from stored-data
        :return: None
        """
        self.slots_record = copy.deepcopy(self.slots_record_store)
        self.custom_records = copy.deepcopy(self.custom_records_store)
        self.old_action = copy.deepcopy(self.old_action_store)
        self.pre_action = copy.deepcopy(self.pre_action_store)
        self.cur_action = copy.deepcopy(self.cur_action_store)

    def get_last_message(self):
        """
        Last message from user
        :return: last_message
        """
        return self.last_message

    def get_last_slot_receive(self, name: Text):
        """
        get slot value from last message received
        :param name: name of slot
        :return:value of slot
        """
        if name not in self.slots_record:
            return None
        return self.slots_record[name][-1]

    def get_last_slot_value(self, name: Text):
        """
        get last slot value not None by tracking
        :param name: naoe of slot
        :return:value of slot
        """
        if name not in self.slots_record:
            return None
        for value in reversed(self.slots_record[name]):
            if value is not None:
                return value
        return None

    def get_current_record(self) -> Dict[Text, Text]:
        """
        get value of record
        :return: dictionary of record items
        """
        result = self.user_info
        for name_record in self.custom_records:
            result[name_record] = self.custom_records[name_record].get_value()
        return result

    def get_user_info_record(self) -> Dict[Text, Text]:
        """
        get value of record
        :return: dictionary of record items
        """
        result = self.user_info
        return result

    @classmethod
    def create_recorder(cls,
                        sender_id,
                        old_action,
                        pre_action,
                        cur_action,
                        cur_intent,
                        last_message,
                        custom_records,
                        slots_record,
                        user_info,
                        state
                        ):
        """
        Remake Recorder from value of all attributes
        :param sender_id:
        :param old_action:
        :param pre_action:
        :param cur_action:
        :param cur_intent:
        :param last_message:
        :param custom_records:
        :param slots_record:
        :param user_info:
        :param state:
        :return:
        """
        recorder = cls(None)
        recorder.sender_id = sender_id
        recorder.old_action = old_action
        recorder.pre_action = pre_action
        recorder.cur_action = cur_action
        recorder.cur_intent = cur_intent
        recorder.last_message = last_message
        for key in custom_records:
            recorder.custom_records[key].set_record(custom_records[key])
        recorder.slots_record = slots_record
        recorder.user_info = user_info
        recorder.state = state
        recorder.slots_record_store = copy.deepcopy(recorder.slots_record)
        recorder.custom_records_store = copy.deepcopy(recorder.custom_records)
        recorder.old_action_store = copy.deepcopy(recorder.old_action)
        recorder.pre_action_store = copy.deepcopy(recorder.pre_action)
        recorder.cur_action_store = copy.deepcopy(recorder.cur_action)
        return recorder

    @classmethod
    def from_dict(cls, conversation_id, data):
        """
        Create new Recorder from dict-data
        :param conversation_id: conversation id
        :param data: dict data
        :return: new Recorder
        """
        recorder = Recorder.create_recorder(
            sender_id=conversation_id,
            old_action=data["old_action"],
            pre_action=data["pre_action"],
            cur_action=data["cur_action"],
            cur_intent=data["cur_intent"],
            last_message=data["last_message"],
            custom_records=data["custom_records"],
            slots_record=data["slots_record"],
            user_info=data["user_info"],
            state=data["state"]
        )
        return recorder

    def dumps(self):
        """
        Dump self - Recorder to dict data
        :return: dict of attributes of Recorder
        """
        data = {}
        data["old_action"] = self.old_action
        data["pre_action"] = self.pre_action
        data["cur_action"] = self.cur_action
        data["cur_intent"] = self.cur_intent
        data["last_message"] = self.last_message
        custom_records = {}
        for key in self.custom_records:
            custom_records[key] = self.custom_records[key].get_value()
        data["custom_records"] = custom_records
        data["slots_record"] = self.slots_record
        data["user_info"] = self.user_info
        data["state"] = self.state
        return data


class RecordManager:
    """
    Recorder for all senders
    """

    def __init__(self):
        """
        init dictionary of recorders for each sender
        """
        self.record_per_user = {}

    def get(self, tracker: Tracker) -> Recorder:
        """
        get Recorder by sender_id
        :param tracker: tracker -> tracker.sender_id
        :return: Recorder(sender_id)
        """
        sender_id = tracker.sender_id
        if sender_id not in self.record_per_user:
            self.record_per_user[sender_id] = Recorder(sender_id)
        return self.record_per_user[sender_id]
