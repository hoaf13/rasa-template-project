import os
from config import DIR_RESOURCES

DIR_UTILS = os.path.join(DIR_RESOURCES, "utils")

# For regex special: regex_special.py
DIR_REGEX_SPECIAL = os.path.join(DIR_UTILS, "regex_special")
PATH_REGEX_SPECIAL_NOT_NAMES = os.path.join(DIR_REGEX_SPECIAL, "not_name.txt")
PATH_REGEX_SPECIAL_PROVINCES = os.path.join(DIR_REGEX_SPECIAL, "provinces.txt")
PATH_REGEX_SPECIAL_NOT_ADDRESS = os.path.join(DIR_REGEX_SPECIAL, "not_address.txt")
PATH_REGEX_SPECIAL_CHANGE_DECISION = os.path.join(DIR_REGEX_SPECIAL, "change_decision.txt")
PATH_REGEX_SPECIAL_STOP_WORDS_NOT_USE = os.path.join(DIR_REGEX_SPECIAL, "stopwords_vn_rm.txt")
