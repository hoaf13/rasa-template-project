from typing import Any, Dict, Iterator, List, Optional, Text
import os
import json
import re
import datetime
import random
import logging

from src.services.lunar_date import L2S, S2L

logger = logging.getLogger(__name__)


def normalize_time(_time=None, _session_of_day=None):
    if _time is not None:
        _time = _time.replace("giờ", "h")
        _time = _time.replace("đúng", "")
        _time = _time.replace("hơn", "")
        _time = _time.replace("kém", "")
        _time = _time.replace("giờ rưỡi", "rưỡi")
        _time = _time.replace("rưỡi", "giờ 30")
        _time = _time.replace("giờ", "h")
        while "h0" in _time:
            _time = _time.replace("h0", "h")
        _time = _time.strip()
        if _time.endswith("h"):
            _time = f"{_time}0"
        tmp = _time.split("h")
        hour = int(normalize_number(tmp[0].strip()))
        minute = int(normalize_number(tmp[1].strip()))
        if _session_of_day is not None and (hour < 12 or (hour == 12 and minute == 0)):
            if "trưa" in _session_of_day:
                if hour == 1 or hour == 2:
                    hour += 12
            elif "chiều" in _session_of_day:
                hour += 12
            elif "tối" in _session_of_day or "đêm" in _session_of_day:
                if hour > 4:
                    hour += 12
        if minute > 60:
            minute = 0
        if minute == 0:
            return f"{hour} giờ"
        else:
            return f"{hour} giờ {minute} phút"
    else:
        if _session_of_day is not None:
            patterns = {
                "tối muộn": "tối muộn",
                "đêm muộn": "tối muộn",
                "muộn nhất": "tối muộn",
                "chậm nhất": "tối muộn",
                "chuyến cuối": "tối muộn",
                "sớm nhất": "sáng sớm",
                "chuyến đầu": "sáng sớm"
            }
            if _session_of_day in patterns:
                return patterns[_session_of_day]
            else:
                return _session_of_day
    return None


def trans_date(_date):
    next_days = {
        "hôm nay": 0,
        "ngày mai": 1,
        "ngày mốt": 2,
        "ngày kia": 2,
        "ngày kìa": 3,
        "mai": 1
    }
    weekdays = {
        "thứ 2": 0,
        "thứ 3": 1,
        "thứ 4": 2,
        "thứ 5": 3,
        "thứ 6": 4,
        "thứ 7": 5,
        "thứ hai": 0,
        "thứ ba": 1,
        "thứ tư": 2,
        "thứ bốn": 2,
        "thứ năm": 3,
        "thứ sáu": 4,
        "thứ bảy": 5,
        "thứ bẩy": 5,
        "chủ nhật": 6
    }
    range_days = -1
    if _date in next_days:
        range_days = next_days[_date]
    if _date in weekdays:
        range_days = (7 + weekdays[_date] - datetime.datetime.today().weekday()) % 7
    if range_days >= 0:
        _datetime = datetime.datetime.today() + datetime.timedelta(days=range_days)
        _date = f"{_datetime.strftime('%d/%m/%Y')}"
        return _date, True
    return _date, False


def lunar_to_sun(_date, tet=False):
    _date_norm, text_rep = normalize_date(_date)
    if _date_norm is None:
        return _date
    tmp = _date_norm.split("/")
    lunar_date = int(tmp[0])
    if tet:
        if lunar_date >= 20:
            lunar_month = 12
        else:
            lunar_month = 1
    else:
        lunar_month = int(tmp[1])

    logger.debug(f"DATE LUNAR: {lunar_date} {lunar_month}")

    today = datetime.datetime.today()
    dd = today.day
    mm = today.month
    yyyy = today.year
    _today_moon = S2L(dd, mm, yyyy)
    logger.debug(f"DATE MOON: {_today_moon}")
    # moon_date = _today_moon[0]
    moon_month = _today_moon[1]
    lunar_year = _today_moon[2]
    if moon_month > lunar_month:
        lunar_year += 1
    logger.debug(f"DATE LUNAR: {lunar_date} {lunar_month} {lunar_year}")
    _date_sun = L2S(lunarD=lunar_date, lunarM=lunar_month, lunarY=lunar_year, lunarLeap=0)

    logger.debug(f"DATE SUN: {_date_sun}")
    sun_date = _date_sun[0]
    sun_month = _date_sun[1]
    sun_year = _date_sun[2]
    return f"{sun_date}/{sun_month}/{sun_year}"


def normalize_date(_date):
    text_rep = _date
    if _date is not None:
        for pattern in ["tết", "âm", "tháng giêng", "tháng chạp"]:
            if pattern in _date:
                _date = _date.replace("tháng giêng", "tháng 1").replace("tháng chạp", "tháng 12")
                _date = _date.replace("âm", "tết").replace("tết", "").strip()
                if "tháng" not in _date:
                    return [lunar_to_sun(_date, tet=True), text_rep]
                return [lunar_to_sun(_date), text_rep]
        today = datetime.datetime.today()
        dd = today.day
        mm = today.month
        yyyy = today.year
        for rpl in ["tháng sau", "tháng tới"]:
            _date = _date.replace(rpl, f"tháng {mm + 1}")
        _date, is_rep = trans_date(_date)
        words = _date.replace("/", " ").split(" ")
        _date = ""
        for w in words:
            w = normalize_number(w)
            if not w.isdigit():
                w = " "
            _date = _date + w + " "
        _date = _date.strip()
        while " " in _date:
            _date = _date.replace(" ", "/")
        while "//" in _date:
            _date = _date.replace("//", "/")
        tmp = _date.split("/")
        if len(tmp) == 1:
            _date = int(normalize_number(tmp[0]))
            _month = mm
            _year = yyyy
            if _date < dd:
                _month += 1
            if _month > 12:
                _month = 1
                _year += 1
        elif len(tmp) == 2:
            _date = int(normalize_number(tmp[0]))
            _month = int(normalize_number(tmp[1]))
            _year = yyyy
            if _month < mm:
                _year += 1
        elif len(tmp) == 3:
            _date = int(normalize_number(tmp[0]))
            _month = int(normalize_number(tmp[1]))
            _year = int(normalize_number(tmp[2]))
        else:
            return [None, None]
        if not is_rep:
            text_rep = None
        return [f"{_date}/{_month}/{_year}", text_rep]
    return [None, None]


def normalize_number(_number):
    mp = {
        "một": 1,
        "hai": 2,
        "ba": 3,
        "bốn": 4,
        "tư": 4,
        "năm": 5,
        "sáu": 6,
        "bảy": 7,
        "bẩy": 7,
        "tám": 8,
        "chín": 9,
        "mười": 10,
        "mười một": 11,
        "mười hai": 12,
        "mười ba": 13,
        "mười bốn": 14,
        "mười lăm": 15,
        "mười năm": 15,
        "mười sáu": 16,
        "mười bảy": 17,
        "mười bẩy": 17,
        "mười tám": 18,
        "mười chín": 19,
        "hai mươi": 20,
        "hai một": 21,
        "hai mươi một": 21,
        "hai mốt": 21,
        "hai mươi mốt": 21,
        "hai hai": 22,
        "hai mươi hai": 22,
        "hai ba": 23,
        "hai mươi ba": 23,
        "hai tư": 24,
        "hai mươi tư": 24,
        "hai bốn": 24,
        "hai mươi bốn": 24,
    }
    if _number in mp:
        return f"{mp[_number]}"
    return _number


def get_provide_time(_time, _number, _session_of_day):
    if _time is not None:
        if _session_of_day is not None:
            return normalize_time(_time, _session_of_day)
        return normalize_time(_time=_time)
    else:
        if _session_of_day is not None:
            return normalize_time(_session_of_day=_session_of_day)
        if _number is not None:
            return normalize_time(_time=f"{normalize_number(_number)} giờ")
    return None


def get_provide_date(_date, _time, _number, _session_of_day):
    if _date is not None:
        return normalize_date(_date)
    else:
        if _time is None and _number is not None:
            return normalize_date(f"ngày {normalize_number(_number)}")
    return None


def norm_date_for_request(_date):
    if _date is None:
        return None
    pattern = "\\b([0-9]{1}|[0-9]{2})/([0-9]{1}|[0-9]{2})/[0-9]{4}\\b"
    match = re.search(pattern, _date)
    if match:
        _date = _date[match.start():match.end()]
        tmp = _date.split("/")  # dd/mm/yyyy
        dd = tmp[0]
        if len(dd) == 1:
            dd = "0" + dd
        mm = tmp[1]
        if len(mm) == 1:
            mm = "0" + mm
        yyyy = tmp[2]
        return f"{yyyy}-{mm}-{dd}"
    return ""


def norm_time_for_request(_time):
    # x giờ
    # x giờ y phút
    # tối muộn
    # sáng sớm
    if _time is None:
        return ""
    if _time == "tối muộn":
        return "23:59"
    if _time == "sáng sớm":
        return "00:01"
    if "sáng" in _time:
        return "08:59"
    if "trưa" in _time:
        return "11:59"
    if "chiều" in _time:
        return "14:59"
    if "tối" in _time:
        return "18:59"
    if "đêm" in _time:
        return "23:59"
    logger.debug(f"TIME {_time}")
    _time = _time.replace(" giờ", "h").replace("phút", "").replace(" ", "")
    if _time.endswith("h"):
        _time = f"{_time}00"
    try:
        tmp = _time.split("h")
        logger.debug(f"TIME {tmp}")
        h = str(int(tmp[0]))
        m = str(int(tmp[1]))
        if len(h) == 1:
            h = f"0{h}"
        if len(m) == 1:
            m = f"0{m}"
        return f"{h}:{m}"
    except:
        return "08:00"


def get_range_time(_t1, _t2):
    tmp = _t1.split("h")
    h = int(tmp[0])
    m = int(tmp[1])
    sum_1 = h * 60 + m
    tmp = _t2.split("h")
    h = int(tmp[0])
    m = int(tmp[1])
    sum_2 = h * 60 + m
    return abs(sum_1 - sum_2)
