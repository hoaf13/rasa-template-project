import re
import logging
from builtins import enumerate
from urllib.response import addbase

import copy
from src.utils.config_utils import (
    PATH_REGEX_SPECIAL_CHANGE_DECISION,
    PATH_REGEX_SPECIAL_NOT_ADDRESS,
    PATH_REGEX_SPECIAL_NOT_NAMES,
    PATH_REGEX_SPECIAL_PROVINCES,
    PATH_REGEX_SPECIAL_STOP_WORDS_NOT_USE,
)


def norm_vn_punc(text):
    l1 = ["òa", "óa", "ỏa", "õa", "ọa", "òe", "óe", "ỏe", "õe", "ọe", "ùy", "úy", "ủy", "ũy", "ụy"]
    l2 = ["oà", "oá", "oả", "oã", "oạ", "oè", "oé", "oẻ", "oẽ", "oẹ", "uỳ", "uý", "uỷ", "uỹ", "uỵ"]
    for w1, w2 in zip(l1, l2):
        text = text.replace(w2, w1)
    return text


def has_numbers(text):
    return any(char.isdigit() for char in text)


def has_special_chr(text):
    spc = '[@_!#$%^&*()<>?/\|}{~:]'
    return any(c in spc for c in text)


def replace_space(message):
    while "  " in message:
        message = message.replace("  ", " ")
    return message.strip()


def find_first_entities(list_patterns, message, name):
    ans = None
    for regex_item in list_patterns:
        pattern = re.compile(regex_item)
        for match in pattern.finditer(message.lower()):
            entity = {
                "start": match.start(),
                "end": match.end(),
                "value": match.group(),
                "confidence": 1.0,
                "entity": name,
            }
            if ans is None or ans["start"] > entity["start"]:
                ans = copy.deepcopy(entity)
    return ans


def read_samples_from_file(file_name):
    with open(file_name, "r") as f_in:
        examples = [line.replace("\n", "").strip().lower() for line in f_in.readlines()]
    result = {}
    for example in examples:
        result[example] = 1
    return result


class RegexSpecialCase:
    def __init__(self):
        self.not_names = read_samples_from_file(PATH_REGEX_SPECIAL_NOT_NAMES)
        self.provinces = read_samples_from_file(PATH_REGEX_SPECIAL_PROVINCES)
        self.not_address = read_samples_from_file(PATH_REGEX_SPECIAL_NOT_ADDRESS)
        self.change_decision = read_samples_from_file(PATH_REGEX_SPECIAL_CHANGE_DECISION)
        self.stop_words_not_use = read_samples_from_file(PATH_REGEX_SPECIAL_STOP_WORDS_NOT_USE)

    def process_name_remove(self, message):
        message = message.lower()
        # message = norm_vn_punc(message)
        message = " " + message + " "
        # bỏ từ dư thừa
        for change in self.change_decision:
            if change != "":
                pattern = f" {change} "
                if re.sub(pattern, " [change_decision] ", message):
                    message = re.sub(pattern, " [change_decision] ", message)
        for not_name in self.not_names:
            if not_name != "":
                pattern = f" {not_name} "
                if re.sub(pattern, " [not_name] ", message):
                    message = re.sub(pattern, " [not_name] ", message)

        # for stop_w in self.stop_words_not_use:
        #     if stop_w != "":
        #         pattern = f" {stop_w} "
        #         if re.sub(pattern, " [stop_word] ", message):
        #             message = re.sub(pattern, " [stop_word] ", message)

        message = replace_space(message)
        words = message.split()

        # kiểm tra khi đổi ý
        if "[change_decision]" in words:
            words_tmp = []
            for w in reversed(words):
                if w == "[change_decision]":
                    break
                words_tmp.append(w)
            words_tmp.reverse()
            words = words_tmp
        for i, word in enumerate(words):
            if has_numbers(word) or has_special_chr(word):
                words[i] = "[not_name]"
        logging.debug(f"LOGGING::process_name::words::{words}")
        words_ans = []
        words_tmp = []
        words.append("[not_name]")
        # Lấy tên trong đoạn liên tiếp dài nhất
        for word in words:
            if word in ["[not_name]", "[change_decision]", "[stop_word]"]:
                if len(words_ans) <= len(words_tmp):
                    words_ans = [w for w in words_tmp]
                words_tmp = []
            else:
                words_tmp.append(word)
        words = words_ans
        logging.debug(f"LOGGING::process_name::words::{words}")
        # kiểm tra độ dài của tên, nếu không thỏa mãn trả về rỗng
        if len(words) < 2 or len(words) > 7:
            return {}
        logging.debug(f"LOGGING::process_name::words::{words}")
        ho_va_ten = " ".join(words)
        pattern = re.compile(ho_va_ten)
        name = 'name'
        entities = {}
        for match in pattern.finditer(message.lower()):
            entity = {
                "start": match.start(),
                "end": match.end(),
                "value": match.group(),
                "confidence": 1.0,
                "entity": name,
            }
            if name not in entities:
                entities[name] = []
            entities[name].append(entity)
        return entities

    def process_birthday(self, message, regex_features):
        patterns_date = []
        patterns_month = []
        patterns_year = []
        for patterns_by_entity in regex_features:
            if patterns_by_entity['name'] == "date":
                patterns_date.append(patterns_by_entity['pattern'])
            if patterns_by_entity['name'] == "month":
                patterns_month.append(patterns_by_entity['pattern'])
            if patterns_by_entity['name'] == "year":
                patterns_year.append(patterns_by_entity['pattern'])

        entity_date = find_first_entities(patterns_date, message, "date")
        last_index = 0
        if entity_date is not None:
            last_index = entity_date['end']
        message_tmp = f"{'#' * last_index}{message[last_index:]}"
        entity_month = find_first_entities(patterns_month, message_tmp, "month")
        if entity_month is not None:
            last_index = entity_month['end']
        message_tmp = f"{'#' * last_index}{message[last_index:]}"
        entity_year = find_first_entities(patterns_year, message_tmp, "year")
        entities = {}
        if entity_date is not None:
            entities['date'] = [entity_date]
        if entity_month is not None:
            entities['month'] = [entity_month]
        if entity_year is not None:
            entities['year'] = [entity_year]
        logging.debug(f"LOGGING::process_birthday::entities::{entities}")
        return entities

    def process_address_remove(self, message):
        message = message.lower()
        # message = norm_vn_punc(message)
        message = " " + message + " "
        # bỏ từ dư thừa
        for change in self.change_decision:
            if change != "":
                pattern = f" {change} "
                if re.sub(pattern, " [change_decision] ", message):
                    message = re.sub(pattern, " [change_decision] ", message)
        for not_add in self.not_address:
            if not_add != "":
                pattern = f" {not_add} "
                if re.sub(pattern, " [not_address] ", message):
                    message = re.sub(pattern, " [not_address] ", message)
        for stop_w in self.stop_words_not_use:
            if stop_w != "":
                pattern = f" {stop_w} "
                if re.sub(pattern, " [stop_word] ", message):
                    message = re.sub(pattern, " [stop_word] ", message)

        message = replace_space(message)
        words = message.split()
        # kiểm tra dổi ý
        if "[change_decision]" in words:
            words_tmp = []
            for w in reversed(words):
                if w == "[change_decision]":
                    break
                words_tmp.append(w)
            words_tmp.reverse()
            words = words_tmp
        logging.debug(f"LOGGING::process_address::words::{words}")
        # remove
        words_ans = []
        for word in words:
            if word not in ["[change_decision]", "[not_address]", "[stop_word]"]:
                words_ans.append(word)
        words = words_ans
        logging.debug(f"LOGGING::process_address::words::{words}")
        # kiểm tra có chứa province trong địa chỉ hay không

        address = " ".join(words)
        check_province = False
        new_province = None
        for province in self.provinces:
            if re.search(f"\\b{province}\\b", address):
                check_province = True
                new_province = province
                break
        address = f"{address}|{new_province}"
        if not check_province:
            return {}
        entities = {
            "province": [
                {
                    "start": len(message) - 1,
                    "end": len(address) + len(message),
                    "value": address,
                    "confidence": 1.0,
                    "entity": 'province',
                }
            ]
        }
        logging.debug(f"LOGGING::process_address::entities::{entities}")
        return entities

    @staticmethod
    def process(message, regex_features):
        regex_features = copy.deepcopy(regex_features)
        # entities_name = self.process_name_remove(message)
        # entities_address = self.process_address_remove(message)
        # entities_birthday = self.process_birthday(message, regex_features)
        # return [entities_name]  # , entities_birthday, entities_address]
        return []
