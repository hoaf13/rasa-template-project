import pickle
import json
import logging

from config import PATH_CONFIG_GRAPH, PATH_CONFIG_LIMITS_SLOTS

logger = logging.getLogger(__name__)


def normalization_intents(name_intents):
    """
    Extract name_intent and entities from the name_intents as edge in graph
    - name_intents: name_intent with entities format
    - return: name of intent and name of entities
    """
    name_intent_all = name_intents.split("/")[-1]
    name_intent = name_intent_all.split("{")[0]
    name_entities = []
    if len(name_intent_all.split("{")) > 1:
        entities = name_intent_all.split("{")[-1]
        entities = "{" + entities
        entities = eval(entities)
        for x in entities:
            name_entities.append(x)
    else:
        name_entities = None
    return name_intent, name_entities


class MemoizationGraph(object):
    """docstring for MemoizationGraph
    - Load stories from the file: config/graph
    - Save stories as a map
    - Methods: get_next_action, get_limit_slots
    """

    def __init__(self):
        """self.stories: graph of stories"""
        with open(PATH_CONFIG_GRAPH, "rb") as file_config_graph:
            self.stories = pickle.load(file_config_graph)
        self.graph_stories = {}
        self.save_stories()
        with open(PATH_CONFIG_LIMITS_SLOTS, "r") as file_limits_slot:
            self.limit_slot = json.load(file_limits_slot)

    def get_next_action(self, action_pre, name_intent, name_entities):
        """
        Get next action from self.stories with:
        - action_pre: previous action as previous node in the graph
        - name_intent: intent as edge in the graph
        - name_entities: entities in intent
        """
        if self.graph_stories.get(action_pre) is None:
            return None
        if self.graph_stories.get(action_pre).get(name_intent) is None:
            return None

        name_entities = self.get_name_entities_after_limits_slot(name_intent, name_entities)

        if name_entities is None or len(name_entities) == 0:
            if self.graph_stories.get(action_pre).get(name_intent).get('not_entities') is None:
                return None
            return self.graph_stories.get(action_pre).get(name_intent).get('not_entities').get('action')
        name_entities = sorted(name_entities)

        if self.graph_stories.get(action_pre).get(name_intent).get(tuple(name_entities)) is None:
            return None
        return self.graph_stories.get(action_pre).get(name_intent).get(tuple(name_entities)).get('action')

    def get_name_entities_after_limits_slot(self, name_intent, name_entities):
        if self.limit_slot.get(name_intent) is None:
            return None
        if name_entities is None or len(name_entities) == 0:
            return None
        result = []
        for entity in self.limit_slot.get(name_intent):
            if entity in name_entities:
                result.append(entity)
        return result

    def save_stories(self):
        """
        Save stories as a graph_stories - a map with key [pre_node, name_intent, entities]
        """
        for data in self.stories:
            pre_node = data['pre_node']
            intent = data['intent']
            cur_node = data['cur_node']
            name_intent, name_entities = normalization_intents(intent)
            if name_entities is None:
                if self.graph_stories.get(pre_node) is None:
                    self.graph_stories[pre_node] = {}
                    self.graph_stories[pre_node][name_intent] = {}
                    self.graph_stories[pre_node][name_intent]['not_entities'] = {}
                    self.graph_stories[pre_node][name_intent]['not_entities']['action'] = {}
                    self.graph_stories[pre_node][name_intent]['not_entities']['action'] = cur_node
                else:
                    if self.graph_stories.get(pre_node).get(name_intent) is None:
                        self.graph_stories[pre_node][name_intent] = {}
                        self.graph_stories[pre_node][name_intent]['not_entities'] = {}
                        self.graph_stories[pre_node][name_intent]['not_entities']['action'] = {}
                        self.graph_stories[pre_node][name_intent]['not_entities']['action'] = cur_node
                    else:
                        if self.graph_stories.get(pre_node).get(name_intent).get('not_entities') is None:
                            self.graph_stories[pre_node][name_intent]['not_entities'] = {}
                            self.graph_stories[pre_node][name_intent]['not_entities']['action'] = {}
                            self.graph_stories[pre_node][name_intent]['not_entities']['action'] = cur_node
                        else:
                            if self.graph_stories.get(pre_node).get(pre_node).get('not_entities').get(
                                    'action') != cur_node:
                                logger.debug("Fail in saving graph")
                                return 0
            else:
                name_entities = sorted(name_entities)
                if self.graph_stories.get(pre_node) is None:
                    self.graph_stories[pre_node] = {}
                    self.graph_stories[pre_node][name_intent] = {}
                else:
                    if self.graph_stories.get(pre_node).get(name_intent) is None:
                        self.graph_stories[pre_node][name_intent] = {}
                if self.graph_stories[pre_node][name_intent].get(tuple(name_entities)) is None:
                    self.graph_stories[pre_node][name_intent][tuple(name_entities)] = {}
                    self.graph_stories[pre_node][name_intent][tuple(name_entities)]['action'] = cur_node
                else:
                    if self.graph_stories[pre_node][name_intent][tuple(name_entities)].get('action') != cur_node:
                        logger.debug("Fail in saving graph")
                        return 0
