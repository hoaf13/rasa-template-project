from rasa.core.domain import Domain
import pickle
import logging

from config import PATH_CONFIG_GRAPH, PATH_DOMAIN

logger = logging.getLogger(__name__)


class IntentLimitation(object):
    def __init__(self):
        if PATH_CONFIG_GRAPH is None:
            logger.debug("path config not exits")
        else:
            with open(PATH_CONFIG_GRAPH, "rb") as file_config_graph:
                self.stories = pickle.load(file_config_graph)
        if PATH_DOMAIN is None:
            logger.debug("path domain not exits")
        else:
            self.domain_intents = Domain.load(PATH_DOMAIN).intents

    @staticmethod
    def normalize_intent(name_intents):
        # get name intent.
        name_intents = name_intents.split("/")[-1]
        name_intents = name_intents.split("{")[0]
        return name_intents

    def get_intent(self, pre_action):
        # prediction next limit intents
        limit_intents = []
        if pre_action is None:
            pre_action = "START"
        for data in self.stories:
            if data['pre_node'] == pre_action:
                intent = self.normalize_intent(data['intent'])
                if intent in self.domain_intents and intent not in limit_intents:
                    limit_intents.append(intent)
        return limit_intents
