import zlib
import os
import base64
import json
import logging
from tqdm import tqdm
from typing import Optional, Any, Dict, List, Text

import rasa.utils.io

from rasa.core.domain import Domain
from rasa.core.events import ActionExecuted
from rasa.core.featurizers import TrackerFeaturizer, MaxHistoryTrackerFeaturizer
from rasa.core.policies.policy import Policy
from rasa.core.trackers import DialogueStateTracker
from rasa.utils.common import is_logging_disabled
from rasa.core.constants import MEMOIZATION_POLICY_PRIORITY

import numpy as np
from src.policies.memoization_graph import MemoizationGraph
from src.nlu.classifier.regex_classifier import RegexIntentClassifier
from src.policies.limit_slot import SlotLimitation
from src.context.recorder import Recorder
from src.policies.limit_intent import IntentLimitation
from src.database.context_store import RedisStore, ContextStore

from config import (
    PATH_CONFIG,
    PATH_CONFIG_FLOW_INTENT,
    PATH_CONFIG_STOP_WORDS,
    MAX_INTENTS
)

logger = logging.getLogger(__name__)


class MemoizationPolicy(Policy):
    """The policy that remembers exact examples of
        `max_history` turns from training stories.

        Since `slots` that are set some time in the past are
        preserved in all future feature vectors until they are set
        to None, this policy implicitly remembers and most importantly
        recalls examples in the context of the current dialogue
        longer than `max_history`.

        This policy is not supposed to be the only policy in an ensemble,
        it is optimized for precision and not recall.
        It should get a 100% precision because it emits probabilities of 1.1
        along it's predictions, which makes every mistake fatal as
        no other policy can overrule it.

        If it is needed to recall turns from training dialogues where
        some slots might not be set during prediction time, and there are
        training stories for this, use AugmentedMemoizationPolicy.
    """

    ENABLE_FEATURE_STRING_COMPRESSION = True

    SUPPORTS_ONLINE_TRAINING = True

    USE_NLU_CONFIDENCE_AS_SCORE = False

    @staticmethod
    def _standard_featurizer(max_history=None):
        # Memoization policy always uses MaxHistoryTrackerFeaturizer
        # without state_featurizer
        return MaxHistoryTrackerFeaturizer(
            state_featurizer=None,
            max_history=max_history,
            use_intent_probabilities=False,
        )

    def __init__(
            self,
            featurizer: Optional[TrackerFeaturizer] = None,
            priority: int = MEMOIZATION_POLICY_PRIORITY,
            max_history: Optional[int] = None,
            lookup: Optional[Dict] = None,
    ) -> None:

        if not featurizer:
            featurizer = self._standard_featurizer(max_history)

        super().__init__(featurizer, priority)
        with open(PATH_CONFIG, "r") as file_config:
            self.config = json.load(file_config)
            self.fallback_intents = self.config.get('fallback_intents')
            self.break_fallback_again = self.config.get('break_fallback_again')
            self.confidence_threshold = self.config.get('confidence_threshold')
            self.confidence_threshold_short_message = self.config.get('confidence_threshold_short_message')
            self.intent_fixed = self.config.get("fixed_intents")
        self.slots_limit = SlotLimitation()
        self.intent_limit = IntentLimitation()
        self.regex_classifier = RegexIntentClassifier()
        self.memoization_graph = MemoizationGraph()
        self.max_history = self.featurizer.max_history
        self.lookup = lookup if lookup is not None else {}
        self.is_enabled = True
        with open(PATH_CONFIG_FLOW_INTENT, "r") as flow_intent:
            self.flow_intent = json.load(flow_intent)
        with open(PATH_CONFIG_STOP_WORDS, "r") as stop_words:
            self.stop_words = json.load(stop_words)
        self.redis_more_store = RedisStore()

    def toggle(self, activate: bool) -> None:
        self.is_enabled = activate

    def _add_states_to_lookup(
            self, trackers_as_states, trackers_as_actions, domain, online=False
    ):
        """Add states to lookup dict"""
        if not trackers_as_states:
            return

        assert len(trackers_as_states[0]) == self.max_history, (
            "Trying to mem featurized data with {} historic turns. Expected: "
            "{}".format(len(trackers_as_states[0]), self.max_history)
        )

        assert len(trackers_as_actions[0]) == 1, (
            "The second dimension of trackers_as_action should be 1, "
            "instead of {}".format(len(trackers_as_actions[0]))
        )

        ambiguous_feature_keys = set()

        pbar = tqdm(
            zip(trackers_as_states, trackers_as_actions),
            desc="Processed actions",
            disable=is_logging_disabled(),
        )
        for states, actions in pbar:
            action = actions[0]

            feature_key = self._create_feature_key(states)
            feature_item = domain.index_for_action(action)

            if feature_key not in ambiguous_feature_keys:
                if feature_key in self.lookup.keys():
                    if self.lookup[feature_key] != feature_item:
                        if online:
                            logger.info(
                                "Original stories are "
                                "different for {} -- {}\n"
                                "Memorized the new ones for "
                                "now. Delete contradicting "
                                "examples after exporting "
                                "the new stories."
                                "".format(states, action)
                            )
                            self.lookup[feature_key] = feature_item
                        else:
                            # delete contradicting example created by
                            # partial history augmentation from memory
                            ambiguous_feature_keys.add(feature_key)
                            del self.lookup[feature_key]
                else:
                    self.lookup[feature_key] = feature_item
            pbar.set_postfix({"# examples": "{:d}".format(len(self.lookup))})

    def _create_feature_key(self, states: List[Dict]) -> Text:
        from rasa.utils import io

        feature_str = json.dumps(states, sort_keys=True).replace('"', "")
        if self.ENABLE_FEATURE_STRING_COMPRESSION:
            compressed = zlib.compress(bytes(feature_str, io.DEFAULT_ENCODING))
            return base64.b64encode(compressed).decode(io.DEFAULT_ENCODING)
        else:
            return feature_str

    def train(
            self,
            training_trackers: List[DialogueStateTracker],
            domain: Domain,
            **kwargs: Any,
    ) -> None:
        """Trains the policy on given training trackers."""
        self.lookup = {}
        # only considers original trackers (no augmented ones)
        training_trackers = [
            t
            for t in training_trackers
            if not hasattr(t, "is_augmented") or not t.is_augmented
        ]
        (
            trackers_as_states,
            trackers_as_actions,
        ) = self.featurizer.training_states_and_actions(training_trackers, domain)
        self._add_states_to_lookup(trackers_as_states, trackers_as_actions, domain)
        logger.debug("Memorized {} unique examples.".format(len(self.lookup)))

    def continue_training(
            self,
            training_trackers: List[DialogueStateTracker],
            domain: Domain,
            **kwargs: Any,
    ) -> None:

        # add only the last tracker, because it is the only new one
        (
            trackers_as_states,
            trackers_as_actions,
        ) = self.featurizer.training_states_and_actions(training_trackers[-1:], domain)
        self._add_states_to_lookup(trackers_as_states, trackers_as_actions, domain)

    def _recall_states(self, states: List[Dict[Text, float]]) -> Optional[int]:

        return self.lookup.get(self._create_feature_key(states))

    def recall(
            self,
            states: List[Dict[Text, float]],
            tracker: DialogueStateTracker,
            domain: Domain,
    ) -> Optional[int]:

        return self._recall_states(states)

    def change_confidence_if_start(self, tracker: DialogueStateTracker, recorder: Recorder) -> None:
        if recorder.cur_action is None:
            check = False
            # history save position node in graph
            # if user is first node then intent is greet
            for i, intent_tmp in enumerate(tracker.latest_message.parse_data['intent_ranking']):
                # if intent_tmp['name'] == "greet":
                if intent_tmp['name'] in self.config.get('start_intents'):
                    tracker.latest_message.parse_data['intent_ranking'][i]['confidence'] = 1.0
                    check = True
                    break
            if check is False:
                tracker.latest_message.parse_data['intent_ranking'].append({'name': 'null_intent', 'confidence': 1.0})

    def change_flow_by_user_info(self, tracker: DialogueStateTracker, pre_action, cur_intent, recorder: Recorder):
        """
        :param tracker: Tracker from rasa
        :param pre_action: previous action
        :param cur_intent: intent_predict after run intent_classifier
        :param recorder: previous recorder
        :return: new intent after change flow
        """

        def re_update(new_intent_name):
            logger.debug(f"{tracker.sender_id} -  + Change flow intent to {new_intent}")
            new_name_entities = self.slots_limit.get_name_entities(tracker, new_intent_name)
            predicted_action = self.memoization_graph.get_next_action(pre_action, new_intent, new_name_entities)
            if new_intent_name not in self.intent_fixed:
                recorder.roll_back()
                recorder.update(tracker, predicted_action, new_intent, update_record=True)
            else:
                recorder.update(tracker, predicted_action, new_intent, update_record=False)
            new_record = recorder.get_current_record()
            return predicted_action, new_record

        def check_context(context, limits, record):
            context_param = context["param"]
            if context_param in record:
                context_value = record[context_param]
            else:
                context_value = None
            if context["name"] in limits and context["value"] == context_value:
                return context["name"]
            return None

        last_intent = recorder.cur_intent
        limit_intents = self.intent_limit.get_intent(recorder.cur_action)
        recorder.store()
        cur_name_entities = self.slots_limit.get_name_entities(tracker, cur_intent)
        action_predict = self.memoization_graph.get_next_action(pre_action, cur_intent, cur_name_entities)
        recorder.update(tracker, action_predict, cur_intent)
        cur_record = recorder.get_current_record()
        new_intent = cur_intent
        check_change_flow = False
        for loop in range(2):
            while True:
                # catch flow for context of some intent (in flow_intent.json)
                if cur_intent in self.flow_intent:
                    for tmp_intent in self.flow_intent[cur_intent]["new_intents"]:
                        checked_intent = check_context(
                            context=tmp_intent,
                            limits=limit_intents,
                            record=cur_record
                        )
                        if checked_intent is not None:
                            new_intent = checked_intent
                            check_change_flow = True
                            break
                    if cur_intent == new_intent:
                        break
                    cur_intent = new_intent
                    action_predict, cur_record = re_update(new_intent)
                else:
                    break
            if loop == 1:
                break
            while True:
                # catch flow for context of all intent
                for tmp_intent in self.flow_intent["ANY"]["new_intents"]:
                    checked_intent = check_context(
                        context=tmp_intent,
                        limits=limit_intents,
                        record=cur_record
                    )
                    if checked_intent is not None:
                        new_intent = checked_intent
                        check_change_flow = True
                        break
                if cur_intent == new_intent:
                    break
                cur_intent = new_intent
                action_predict, cur_record = re_update(new_intent)

        if (recorder.old_action == recorder.pre_action) \
                and (new_intent in self.fallback_intents) \
                and (last_intent in self.fallback_intents) \
                and (self.break_fallback_again is True) \
                and ("intent_fallback_again" in limit_intents):
            new_intent = "intent_fallback_again"
            check_change_flow = True
            action_predict, cur_record = re_update(new_intent)

        return action_predict, new_intent, check_change_flow

    def change_confidence_by_custom_model(self, tracker: DialogueStateTracker, recorder: Recorder) -> None:
        sender_id = tracker.sender_id
        intent_real = {"name": "", "confidence": 0}

        intent_pre = self.intent_limit.get_intent(recorder.cur_action)  # prediction intents will occur
        logger.debug(f"{sender_id} - Intent limit area: {intent_pre}")
        logger.debug(f"{sender_id} - Intent ranking passed NLU: {tracker.latest_message.parse_data['intent_ranking']}")
        message = tracker.latest_message.parse_data['text']

        # set up parameter intent classification [confidence].
        for intent_name in intent_pre:
            check_in = False
            for i, intent_tmp in enumerate(tracker.latest_message.parse_data['intent_ranking']):
                if intent_tmp["name"] == intent_name:
                    check_in = True
                    break
            if not check_in:
                tracker.latest_message.parse_data['intent_ranking']. \
                    append({"name": intent_name, "confidence": 0.0})
        state_regex = MAX_INTENTS
        for i, intent_tmp in enumerate(tracker.latest_message.parse_data['intent_ranking']):
            if intent_tmp['name'] not in intent_pre:
                tracker.latest_message.parse_data['intent_ranking'][i]['confidence'] = 0
            else:
                label_regex, idx_regex = self.regex_classifier.predict_name_intent(message, intent_tmp['name'])
                if label_regex is not None and state_regex > idx_regex:
                    tracker.latest_message.parse_data['intent_ranking'][i]['confidence'] = 1.0
                    intent_real['confidence'] = 1.0
                    intent_real['name'] = intent_tmp['name']
                    state_regex = idx_regex
                else:
                    if intent_real['confidence'] < intent_tmp['confidence']:
                        intent_real = intent_tmp
                    else:
                        tracker.latest_message.parse_data['intent_ranking'][i]['confidence'] = 0.0

        logger.debug(f"{sender_id} - Intent passed Regex Classification: {intent_real}")

        # Process intent fallback
        threshold = self.confidence_threshold
        words = message.strip().split(" ")
        len_words = 0
        for w in words:
            if w not in self.stop_words:
                len_words += 1
        is_short_message = len_words <= 1
        if is_short_message:
            threshold = self.confidence_threshold_short_message
        if intent_real['confidence'] < threshold:
            intent_real['name'] = "intent_fallback"
            intent_real['confidence'] = 1.0
            for i, intent_tmp in enumerate(tracker.latest_message.parse_data['intent_ranking']):
                if intent_tmp['name'] == intent_real['name']:
                    tracker.latest_message.parse_data['intent_ranking'][i]['confidence'] = 1.0
                else:
                    tracker.latest_message.parse_data['intent_ranking'][i]['confidence'] = 0.0

        tracker.latest_message.parse_data['intent'] = intent_real
        logger.debug(f"{sender_id} - Intent passed Fallback Policy: {intent_real}")

    def change_slots_with_slots_limit(self, tracker: DialogueStateTracker, intent_name) -> None:
        # Process slot limit
        limited_slots = self.slots_limit.get_limited_slots(intent_name)
        self.slots_limit.remove_unallowed_slots_from_tracker(tracker, limited_slots)
        logger.debug(f"{tracker.sender_id} - Slots after slot limit: {tracker.current_slot_values()}")
        logger.debug(f"{tracker.sender_id} - Entities after slot limit: {tracker.latest_message.entities}")

    def predict_action_probabilities(
            self, tracker: DialogueStateTracker, domain: Domain
    ) -> List[float]:
        """Predicts the next action the bot should take
            after seeing the tracker.

            Returns the list of probabilities for the next actions.
            If memorized action was found returns 1.1 for its index,
            else returns 0.0 for all actions."""
        result = [0.0] * domain.num_actions
        if not self.is_enabled:
            return result
        sender_id = tracker.sender_id  # get id of user send message
        recorder = self.redis_more_store.get_data(sender_id).tickets["recorder"]

        if recorder.state is True and not tracker.latest_message.parse_data["text"].startswith('/'):
            self.change_confidence_if_start(tracker, recorder)
            self.change_confidence_by_custom_model(tracker, recorder)
            # state of set up parameter of intent rank

        cur_intent = tracker.latest_message.parse_data['intent']['name']
        cur_entities = self.slots_limit.get_name_entities(tracker, cur_intent)
        pre_action = recorder.cur_action
        if recorder.cur_action is None:
            pre_action = "START"
        if recorder.state is False:
            pre_action = "action_listen"

        predicted_action = self.memoization_graph.get_next_action(pre_action, cur_intent, cur_entities)
        if recorder.state is True:
            if predicted_action is None:
                predicted_action = "action_quit"
            else:
                logger.debug(f"{tracker.sender_id} - Flow predict BEFORE change flow with context: " +
                             f"> {pre_action} > {cur_intent} > {predicted_action}")
                predicted_action, new_intent, change_flow = \
                    self.change_flow_by_user_info(tracker,
                                                  pre_action,
                                                  cur_intent,
                                                  recorder)
                logger.debug(f"{tracker.sender_id} - Flow predict AFTER change flow with context: " +
                             f"> {pre_action} > {new_intent} > {predicted_action}")
                self.change_slots_with_slots_limit(tracker, new_intent)
            index_action = domain.action_names.index(predicted_action)
            result[index_action] = 1.0
        else:
            index_action = domain.action_names.index("action_listen")
            result[index_action] = 1.0

        result_np = result
        index = np.where(result_np)[0]
        check_end_conversation = False
        if index is not None and len(index) > 0:
            index = index[0].tolist()
            next_action = domain.action_names[index]
            if next_action in domain.user_actions:
                if next_action in self.config.get('finish_actions'):
                    check_end_conversation = True
                else:
                    recorder.state = False
            else:
                recorder.state = True
        else:
            check_end_conversation = True
        if check_end_conversation:
            logger.debug(f"{tracker.sender_id} - Finish conversation!")
        self.redis_more_store.save_data(ContextStore(sender_id, recorder=recorder))
        logger.debug(f"{tracker.sender_id} - Save record data to Redis DB")
        recorder.update_report_to_db()
        return result

    def persist(self, path: Text) -> None:

        self.featurizer.persist(path)

        memorized_file = os.path.join(path, "memorized_turns.json")
        data = {
            "priority": self.priority,
            "max_history": self.max_history,
            "lookup": self.lookup,
        }
        rasa.utils.io.create_directory_for_file(memorized_file)
        rasa.utils.io.dump_obj_as_json_to_file(memorized_file, data)

    @classmethod
    def load(cls, path: Text) -> "MemoizationPolicy":

        featurizer = TrackerFeaturizer.load(path)
        memorized_file = os.path.join(path, "memorized_turns.json")
        if os.path.isfile(memorized_file):
            data = json.loads(rasa.utils.io.read_file(memorized_file))
            return cls(
                featurizer=featurizer, priority=data["priority"], lookup=data["lookup"]
            )
        else:
            logger.info(
                "Couldn't load memoization for policy. "
                "File '{}' doesn't exist. Falling back to empty "
                "turn memory.".format(memorized_file)
            )
            return cls()


class AugmentedMemoizationPolicy(MemoizationPolicy):
    """The policy that remembers examples from training stories
        for `max_history` turns.

        If it is needed to recall turns from training dialogues
        where some slots might not be set during prediction time,
        add relevant stories without such slots to training data.
        E.g. reminder stories.

        Since `slots` that are set some time in the past are
        preserved in all future feature vectors until they are set
        to None, this policy has a capability to recall the turns
        up to `max_history` from training stories during prediction
        even if additional slots were filled in the past
        for current dialogue.
    """

    @staticmethod
    def _back_to_the_future_again(tracker):
        """Send Marty to the past to get
            the new featurization for the future"""

        idx_of_first_action = None
        idx_of_second_action = None

        # we need to find second executed action
        for e_i, event in enumerate(tracker.applied_events()):
            # find second ActionExecuted
            if isinstance(event, ActionExecuted):
                if idx_of_first_action is None:
                    idx_of_first_action = e_i
                else:
                    idx_of_second_action = e_i
                    break

        if idx_of_second_action is None:
            return None
        # make second ActionExecuted the first one
        events = tracker.applied_events()[idx_of_second_action:]
        if not events:
            return None

        mcfly_tracker = tracker.init_copy()
        for e in events:
            mcfly_tracker.update(e)

        return mcfly_tracker

    def _recall_using_delorean(self, old_states, tracker, domain):
        """Recursively go to the past to correctly forget slots,
            and then back to the future to recall."""

        logger.debug("Launch DeLorean...")
        mcfly_tracker = self._back_to_the_future_again(tracker)
        while mcfly_tracker is not None:
            tracker_as_states = self.featurizer.prediction_states(
                [mcfly_tracker], domain
            )
            states = tracker_as_states[0]

            if old_states != states:
                # check if we like new futures
                memorised = self._recall_states(states)
                if memorised is not None:
                    logger.debug(f"Current tracker state {states}")
                    return memorised
                old_states = states

            # go back again
            mcfly_tracker = self._back_to_the_future_again(mcfly_tracker)

        # No match found
        logger.debug(f"Current tracker state {old_states}")
        return None

    def recall(
            self,
            states: List[Dict[Text, float]],
            tracker: DialogueStateTracker,
            domain: Domain,
    ) -> Optional[int]:

        recalled = self._recall_states(states)
        if recalled is None:
            # let's try a different method to recall that tracker
            return self._recall_using_delorean(states, tracker, domain)
        else:
            return recalled
