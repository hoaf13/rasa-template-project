import rasa
from numpy.distutils.cpuinfo import key_value_from_command
from rasa.core.channels.channel import UserMessage, CollectingOutputChannel, InputChannel, \
    QueueOutputChannel

import inspect
import logging
from asyncio import Queue, CancelledError
from sanic import Blueprint, response
from sanic.request import Request
from typing import Text, Dict, Any, Optional, Callable, Awaitable

import rasa.utils.endpoints
from sanic.response import HTTPResponse
from rasa.core.domain import Domain
import asyncio
import pickle
import json

from config import \
    PATH_CONFIG, \
    PATH_DOMAIN, \
    PATH_CONFIG_USER_INFO, \
    PATH_CONFIG_TEMPLATE_SLOTS, \
    PATH_CONFIG_GRAPH, \
    PATH_CONFIG_TEMPLATE_ACTION

from src.database.database_connection import DatabaseConnection
from src.database.user_store import UserInfoStore

logger = logging.getLogger(__name__)


class RestCallbot(InputChannel):
    """A custom http input channel.

    This implementation is the basis for a custom implementation of a chat
    frontend. You can customize this to send messages to Rasa Core and
    retrieve responses from the agent."""

    def __init__(self):
        with open(PATH_CONFIG, "r") as file_config:
            self.config = json.load(file_config)
        self.history_table = self.config.get("database").get("history_table")
        self.start_actions = self.config.get("start_actions")
        self.start_intents = self.config.get("start_intents")
        self.user_information_table = self.config.get("database").get("user_information_table")
        self.path_domain = PATH_DOMAIN
        self.domain = Domain.load(self.path_domain)

        self.connect = DatabaseConnection(host=self.config.get("database").get("host"),
                                          username=self.config.get("database").get("username"),
                                          password=self.config.get("database").get("password"),
                                          db=self.config.get("database").get("db"),
                                          port=self.config.get("database").get("port"))
        with open(PATH_CONFIG_USER_INFO, "r") as file_user_info:
            self.reader_config_user_info = json.load(file_user_info)
            self.PARAMS_INT = []
            self.PARAMS_STR = []
            self.PARAMS_ARR = []
            self.PARAMS_BOOL = []
            for key in self.reader_config_user_info:
                value = self.reader_config_user_info.get(key)
                if isinstance(value, int):
                    self.PARAMS_INT.append(key)
                elif isinstance(value, str):
                    self.PARAMS_STR.append(key)
                elif isinstance(value, list):
                    self.PARAMS_ARR.append(key)
                elif isinstance(value, bool):
                    self.PARAMS_BOOL.append(key)

        with open(PATH_CONFIG_TEMPLATE_SLOTS, "r") as file_temp_slots:
            self.config_slots = json.load(file_temp_slots)

        with open(PATH_CONFIG_GRAPH, "rb") as file_config:
            self.result_stories = pickle.load(file_config)
        with open(PATH_CONFIG_TEMPLATE_ACTION, "r") as file:
            self.data_template = json.load(file)

        self.user_info_store = UserInfoStore()

    @classmethod
    def name(cls) -> Text:
        return "rest_custom"

    @staticmethod
    async def on_message_wrapper(
            on_new_message: Callable[[UserMessage], Awaitable[Any]],
            text: Text,
            queue: Queue,
            sender_id: Text,
            input_channel: Text,
            metadata: Optional[Dict[Text, Any]],
    ) -> None:
        collector = QueueOutputChannel(queue)

        message = UserMessage(
            text, collector, sender_id, input_channel=input_channel, metadata=metadata
        )
        await on_new_message(message)

        await queue.put("DONE")  # py_type: disable=bad-return-type

    async def _extract_sender(self, req: Request) -> Optional[Text]:
        return req.json.get("sender", None)

    # noinspection PyMethodMayBeStatic
    def _extract_message(self, req: Request) -> Optional[Text]:
        return req.json.get("message", None)

    def _extract_input_channel(self, req: Request) -> Text:
        return req.json.get("input_channel") or self.name()

    def stream_response(
            self,
            on_new_message: Callable[[UserMessage], Awaitable[None]],
            text: Text,
            sender_id: Text,
            input_channel: Text,
            metadata: Optional[Dict[Text, Any]],
    ) -> Callable[[Any], Awaitable[None]]:
        async def stream(resp: Any) -> None:
            q = Queue()
            task = asyncio.ensure_future(
                self.on_message_wrapper(
                    on_new_message, text, q, sender_id, input_channel, metadata
                )
            )
            result = None  # declare variable up front to avoid py_type error
            while True:
                result = await q.get()
                if result == "DONE":
                    break
                else:
                    await resp.write(json.dumps(result) + "\n")
            await task

        return stream  # py_type: disable=bad-return-type

    def blueprint(
            self, on_new_message: Callable[[UserMessage], Awaitable[None]]
    ) -> Blueprint:
        custom_webhook = Blueprint(
            "custom_webhook_{}".format(type(self).__name__),
            inspect.getmodule(self).__name__,
        )

        # noinspection PyUnusedLocal
        @custom_webhook.route("/", methods=["GET"])
        async def health(request: Request) -> HTTPResponse:
            return response.json({"status": "ok"})

        # getIntents
        @custom_webhook.route("/getIntents", methods=["GET"])
        async def get_intents(request: Request) -> HTTPResponse:
            list_intents = self.domain.regex_config
            return response.json({
                "status": 0,
                "msg": "Success",
                "intents": list_intents,
            })

        # getEntities
        @custom_webhook.route("/getEntities", methods=["GET"])
        async def get_entities(request: Request) -> HTTPResponse:
            list_entities = self.domain.entities
            print(self.domain)
            return response.json({
                "status": 0,
                "msg": "Success",
                "entities": list_entities,
            })

        # getActions
        @custom_webhook.route("/getActions", methods=["GET"])
        async def get_actions(request: Request) -> HTTPResponse:
            list_actions = self.domain.user_actions
            return response.json({
                "status": 0,
                "msg": "Success",
                "actions": list_actions,
            })

        # inputSlots
        @custom_webhook.route("/inputSlots", methods=["GET"])
        async def input_slots(request: Request) -> HTTPResponse:
            list_slots = [key for key in self.reader_config_user_info]

            return response.json({
                "status": 0,
                "msg": "Success",
                "inputSlots": list_slots,
            })

        @custom_webhook.route("/updateSlots", methods=["POST"])
        async def update_slots(request: Request) -> HTTPResponse:
            id_conversation = request.json.get("id_conversation")

            name_slot = [key for key in self.reader_config_user_info]
            slots_update = request.json.get("input_slots")
            if len(slots_update) == 0:
                return response.json({
                    "status": 0,
                    "msg": "Success",
                })

            set_slots = ""
            for key in slots_update:
                if key in name_slot:
                    name = key
                    value = slots_update.get(key)
                    set_slots = set_slots + f"{name}='{value}',"
            set_slots = set_slots[:-1]
            sql = f"UPDATE {self.user_information_table} " \
                  f"SET {set_slots} " \
                  f"WHERE id_conversation='{id_conversation}'"

            connection = self.connect.get_connection()
            mycursor = connection.cursor()
            mycursor.execute(sql)
            connection.commit()
            mycursor.close()
            connection.close()

            return response.json({
                "status": 0,
                "msg": "Success",
            })

        # getTemplateAction
        @custom_webhook.route("/getTemplateAction", methods=["POST"])
        async def get_template_action(request: Request) -> HTTPResponse:
            def get_key_from_text(text):
                keys = []
                flag = False
                slot = ""
                for c in text:
                    if c == "}":
                        flag = False
                        keys.append(slot)
                    if flag:
                        slot += c
                    if c == "{":
                        flag = True
                        slot = ""
                return keys

            def process_user_info(key, value_info):
                if value_info is None:
                    return "none value"
                if key in ["get_phone_split"]:
                    phone = value_info[:4] + "," + value_info[4:]
                    phone = " ".join(phone)
                    return phone
                return value_info

            def get_list_text_format(text, record_tmp):
                res = []
                res_dynamic = []
                sentences = [sent.strip() for sent in text.split(".")]
                for sentence in sentences:
                    keys = get_key_from_text(sentence)
                    if len(keys) == 0:
                        if sentence != "":
                            res.append(sentence)
                        continue
                    list_format = [sentence]
                    is_dynamic = False
                    for key in keys:
                        if key in self.config_slots:
                            slot = self.config_slots.get(key)
                            source = slot.get("source")
                            values = slot.get("value")
                            if source == "record":
                                tmp = []
                                for val in values:
                                    for sent in list_format:
                                        sent_format = sent.replace("{" + str(key) + "}", val)
                                        if "{" in sent_format:
                                            list_sent_format, _ = get_list_text_format(sent_format, record_tmp)
                                            for sent_format in list_sent_format:
                                                tmp.append(sent_format)
                                        else:
                                            tmp.append(sent_format)
                                list_format = tmp
                                is_dynamic = True
                            else:
                                tmp = []
                                for val in values:
                                    values_user_info = record_tmp.get(val)
                                    if not isinstance(values_user_info, list):
                                        values_user_info = [values_user_info]
                                    for val_user_info in values_user_info:
                                        val_user_info = process_user_info(key, val_user_info)
                                        for sent in list_format:
                                            tmp.append(sent.replace("{" + str(key) + "}", val_user_info))
                                list_format = tmp
                    if is_dynamic:
                        res_dynamic.append(sentence)
                    for sent in list_format:
                        res.append(sent)
                return res, res_dynamic

            id_conversation = request.json.get("id_conversation")
            user_info = self.user_info_store.get(id_conversation)
            result_fixed = {}
            result_dynamic = {}
            record = {}
            for name_info in user_info:
                value = user_info.get(name_info)
                if value is not None:
                    record[name_info] = value
            for name_action in self.data_template:
                for template in self.data_template[name_action]:
                    samples_fixed, samples_dynamic = get_list_text_format(template.get("text_tts"), record)
                    for sample in samples_fixed:
                        result_fixed[sample] = 1
                    for sample in samples_dynamic:
                        result_dynamic[sample] = 1
            result_fixed = [key for key in result_fixed]
            result_dynamic = [key for key in result_dynamic]
            return response.json({
                "status": 0,
                "msg": "Success",
                "action_template": result_fixed,
                "action_template_dynamic": result_dynamic,
            })

        # getStories
        @custom_webhook.route("/getStories", methods=["GET"])
        async def get_stories(request: Request) -> HTTPResponse:
            sql = "SELECT pre_action, action_name, intent_name FROM "
            sql += self.history_table
            connection = self.connect.get_connection()
            cursor = connection.cursor()
            connection.commit()
            cursor.execute(sql)
            query_result = cursor.fetchall()
            cursor.close()
            connection.close()
            check = {}
            total = 0

            for x in query_result:
                total += 1
                tmp = "/" + x[2]
                if check.get(x[0]) is None:
                    check[x[0]] = {}
                    check[x[0]][x[1]] = {}
                    check[x[0]][x[1]][tmp] = 1
                else:
                    if check.get(x[0]).get(x[1]) is None:
                        check[x[0]][x[1]] = {}
                        check[x[0]][x[1]][tmp] = 1
                    else:
                        if check.get(x[0]).get(x[1]).get(tmp) is None:
                            check[x[0]][x[1]][tmp] = 1
                        else:
                            check[x[0]][x[1]][tmp] += 1

            result_edges = self.result_stories

            for x in result_edges:
                pre_node = x['pre_node']
                cur_node = x['cur_node']
                intent = x['intent']
                if check.get(pre_node) is not None and check.get(pre_node).get(cur_node) is not None and check.get(
                        pre_node).get(cur_node).get(intent) is not None:
                    x['count'] = check.get(pre_node).get(cur_node).get(intent)
            list_nodes = ["START"] + self.domain.user_actions + ["END"]
            return response.json({
                "status": 0,
                "msg": "Success",
                "nodes": list_nodes,
                "edges": result_edges,
                "total_conversation": total
            })

        # initialize of calling
        @custom_webhook.route("/initCall", methods=["POST"])
        async def get_slots(request: Request) -> HTTPResponse:
            id_conversation = request.json.get("id_conversation")
            name_slot = [key for key in self.reader_config_user_info]
            keyword = ",".join(name_slot)
            tmp_key = []
            for _ in range(len(name_slot) + 1):
                tmp_key.append("%s")
            value_key = ",".join(tmp_key)
            sql = "INSERT INTO "
            if len(name_slot) > 0:
                sql += self.user_information_table + " (id_conversation," + keyword + ") VALUES (" + value_key + ")"
            else:
                sql += self.user_information_table + " (id_conversation) VALUES (" + value_key + ")"
            val = [id_conversation]
            for name in name_slot:
                if request.json.get("input_slots") is not None:
                    if name in self.PARAMS_ARR:
                        val.append(json.dumps(request.json.get("input_slots").get(name)))
                    else:
                        val.append(request.json.get("input_slots").get(name))

            connection = self.connect.get_connection()
            cursor = connection.cursor()
            cursor.execute(sql, tuple(val))
            connection.commit()
            cursor.close()
            connection.close()

            return response.json({
                "status": 0,
                "msg": "Success",
            })

        # getConversation
        @custom_webhook.route("/getConversation", methods=["POST"])
        async def get_conversation(request: Request) -> HTTPResponse:
            id_conversation = request.json.get("id_conversation")
            if id_conversation is None:
                return response.json({
                    "status": -1,
                    "msg": "Fail",
                    "conversation": None,
                })
            sql = "SELECT pre_action, timestamp, intent_name, action_name, text_user, text_bot FROM "
            sql += self.history_table + " WHERE sender_id = %s"
            connection = self.connect.get_connection()
            cursor = connection.cursor()
            connection.commit()
            cursor.execute(sql, [id_conversation])
            query_result = cursor.fetchall()
            result = []
            for x in query_result:
                bot_json = json.loads(x[5])
                if x[3] in self.start_actions and x[2] in self.start_intents:
                    result = [{
                        "pre_action": "START",
                        "timestamp": x[1],
                        "intent_name": x[2],
                        "action_name": x[3],
                        "text_user": x[4],
                        "text_bot": bot_json['text'],
                        "text_tts_bot": bot_json['text_tts'],
                        "status_bot": bot_json['status']
                    }]
                else:
                    result.append({
                        "pre_action": x[0],
                        "timestamp": x[1],
                        "intent_name": x[2],
                        "action_name": x[3],
                        "text_user": x[4],
                        "text_bot": bot_json['text'],
                        "text_tts_bot": bot_json['text_tts'],
                        "status_bot": bot_json['status']
                    })

            sql = f"SELECT report FROM {self.user_information_table} WHERE id_conversation = %s"
            cursor.execute(sql, [id_conversation])
            rows = cursor.fetchall()
            cursor.close()
            connection.close()
            report = None
            if len(rows) > 0 and rows[0][0] is not None:
                report = json.loads(rows[0][0])

            return response.json({
                "status": 0,
                "msg": "Success",
                "conversation": result,
                "report": report,
            })

        @custom_webhook.route("/notuse", methods=["POST"])
        async def receive(request: Request) -> HTTPResponse:
            sender_id = await self._extract_sender(request)
            text = self._extract_message(request)
            should_use_stream = rasa.utils.endpoints.bool_arg(
                request, "stream", default=False
            )
            input_channel = self._extract_input_channel(request)
            metadata = self.get_metadata(request)

            if should_use_stream:
                return response.stream(
                    self.stream_response(
                        on_new_message, text, sender_id, input_channel, metadata
                    ),
                    content_type="text/event-stream",
                )
            else:
                collector = CollectingOutputChannel()
                # noinspection PyBroadException
                try:
                    await on_new_message(
                        UserMessage(
                            text,
                            collector,
                            sender_id,
                            input_channel=input_channel,
                            metadata=metadata,
                        )
                    )
                except CancelledError:
                    logger.error(
                        "Message handling timed out for "
                        "user message '{}'.".format(text)
                    )
                except Exception:
                    logger.exception(
                        "An exception occurred while handling "
                        "user message '{}'.".format(text)
                    )
                return response.json(collector.messages)

        return custom_webhook
